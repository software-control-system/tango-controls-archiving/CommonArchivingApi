package fr.soleil.database;

public interface DBExtractionConst {

    public static final String DB_SEPARATOR = ".";
    public static final String VALUE_SEPARATOR = ", ";

    public static final String NULL = "null";
    public static final String NOT_NULL = "notnull";

    /**
     * The index at which one can expect to find data "read" part in a data array.
     */
    public static final int READ_INDEX = 0;
    /**
     * The index at which one can expect to find data "write" part in a data array.
     */
    public static final int WRITE_INDEX = 1;

}
