package fr.soleil.database.connection;

import java.sql.Connection;
import java.sql.SQLException;

import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public abstract class AbstractDataBaseConnector {

    public static final boolean USE_LOG4JDBC = Boolean.parseBoolean(System.getProperty("log4jdbc.active"));
    private final DataBaseParameters params;
    private final Logger logger = LoggerFactory.getLogger(AbstractDataBaseConnector.class);

    protected HikariDataSource ds;
    private Jdbi jdbi;

    public AbstractDataBaseConnector(final DataBaseParameters params) throws SQLException {
        this.params = params;
        this.buildPool();
    }

    public abstract String getDriver();

    public abstract Connection getConnection() throws SQLException;

    public void commitAndClose(final Connection conn) throws SQLException {
        try {
            if (conn != null && !conn.getAutoCommit()) {
                conn.commit();
                // give back the connection to the pool with default config
                conn.setAutoCommit(true);
                logger.debug("commit done");
            }
        } finally {
            closeConnection(conn);
        }
    }

    public void closeConnection(final Connection conn) {

        try {
            if (conn != null) {
                logger.debug("closing connection");
                conn.close();
            }
        } catch (final SQLException e) {
            // ignore
        }
    }

    public DataBaseParameters getParams() {
        return new DataBaseParameters(params);
    }

    public String getSchema() {
        return params.getSchema();
    }

    public String getName() {
        return params.getName();
    }

    public int getOpenConnections() {
        if (ds != null)
            return ds.getHikariPoolMXBean().getActiveConnections();
        else
            return -1;
    }

    protected void buildPool() {
        logger.debug("building pool with params {}", params);
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(params.getUrl());
        config.setUsername(params.getUser());
        config.setPassword(params.getPassword());
        config.setPoolName(params.getUniqueKey().replaceAll(":", "#"));
        config.setConnectionTimeout(10000);
        config.setIdleTimeout(10000);
        config.setMaximumPoolSize(params.getMaxPoolSize());
        // activate JMX stats, cf https://github.com/brettwooldridge/HikariCP/wiki/MBean-(JMX)-Monitoring-and-Management
        config.setRegisterMbeans(true);

        if (params.getHealthRegistry() != null) {
            config.setHealthCheckRegistry(params.getHealthRegistry());
        }
        if (params.getMetricRegistry() != null) {
            config.setMetricRegistry(params.getMetricRegistry());
        }
        ds = new HikariDataSource(config);
        jdbi = Jdbi.create(ds);
        logger.debug("build pool OK");
    }

    public Jdbi getJdbi() {
        return jdbi;
    }
}
