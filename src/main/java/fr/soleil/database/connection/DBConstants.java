package fr.soleil.database.connection;

public interface DBConstants {
    /**
     * Parameter that represents the MySQL database driver
     */
    public static final String DRIVER_MYSQL = "jdbc:mysql";

	public static final String DRIVER_H2 = "jdbc:h2:mem";
    /**
     * Parameter that represents the Oracle database driver
     */
    public static final String DRIVER_ORACLE = "jdbc:oracle:thin";

    public static final String DRIVER_POSTGRESQL = "jdbc:postgresql";
    /**
     * Port number for the connection
     */
    public static final String ORACLE_PORT = "1521";

    public static final String POSTGRESQL_PORT = "5432";

    public static final String ORACLE_DATASOURCE = "oracle.jdbc.pool.OracleDataSource";

    public static final short DEFAULT_MAX_POOL_SIZE = 5;

    public static final short DEFAULT_MIN_POOL_SIZE = 1;

    public static final int DEFAULT_INACTIVITY_TIMEOUT = 1;

    public static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";

}
