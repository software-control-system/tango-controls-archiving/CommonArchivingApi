package fr.soleil.database.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class OracleDataBaseConnector extends AbstractDataBaseConnector {

    public static final String ALTER_SESSION_SET_EVENTS_10503_TRACE_NAME_CONTEXT_FOREVER_LEVEL_2000 = "ALTER SESSION SET EVENTS '10503 trace name context forever, level 2000'";
    private static final String ALTER_SESSION_SET_NLS_DATE_FORMAT_DD_MM_YYYY_HH24_MI_SS = "alter session set NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS'";
    private static final String ALTER_SESSION_SET_NLS_NUMERIC_CHARACTERS = "alter session set NLS_NUMERIC_CHARACTERS = \". \"";
    private static final String ALTER_SESSION_SET_NLS_TIMESTAMP_FORMAT_DD_MM_YYYY_HH24_MI_SS_FF = "alter session set NLS_TIMESTAMP_FORMAT = 'DD-MM-YYYY HH24:MI:SS.FF'";
    private final Logger logger = LoggerFactory.getLogger(OracleDataBaseConnector.class);

    public OracleDataBaseConnector(final DataBaseParameters params) throws SQLException {
        super(params);
    }

    @Override
    public String getDriver() {
        return DBConstants.DRIVER_ORACLE;
    }


    @Override
    public synchronized Connection getConnection() throws SQLException {
        // logger.debug("get connection IN");
        int nbTry = 1;
        Connection conn = null;
        SQLException occuredError = null;
        do {
            try {
                conn = ds.getConnection();
                // logger.debug("Connection stats: {}", ds.getHikariPoolMXBean());
                alterSession(conn);
                break;
            } catch (final SQLException e) {
                logger.error("get connection error", e);
                occuredError = e;
                if (conn != null) {
                    try {
                        if (!conn.isValid(100000)) {
                            closeConnection(conn);
                        }
                    } catch (final SQLException e1) {
                        occuredError = e1;
                    }
                    nbTry++;
                } else {
                    nbTry = 4;
                }
            }
        } while (nbTry <= 3);

        if (nbTry > 3) {
            closeConnection(conn);
            throw occuredError;
        }
        if (USE_LOG4JDBC) {
            conn = new net.sf.log4jdbc.ConnectionSpy(conn);
        }
        // logger.debug("get connection OUT");
        return conn;
    }

    /**
     * This method is used when connecting an Oracle database. It tunes the
     * connection to the database.
     *
     * @throws SQLException
     */
    private void alterSession(final Connection con) throws SQLException {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.executeQuery(ALTER_SESSION_SET_NLS_NUMERIC_CHARACTERS);
            stmt.executeQuery(ALTER_SESSION_SET_NLS_TIMESTAMP_FORMAT_DD_MM_YYYY_HH24_MI_SS_FF);
            stmt.executeQuery(ALTER_SESSION_SET_NLS_DATE_FORMAT_DD_MM_YYYY_HH24_MI_SS);
            // apply solution defined here https://support.oracle.com/epmos/faces/DocumentDisplay?_afrLoop=453105751137886&id=2702429.1&displayIndex=1&_afrWindowMode=0&_adf.ctrl-state=itu8b5no_4#FIX
            // to fix to many logs "BIND MISMATCH" on database side
            stmt.execute(ALTER_SESSION_SET_EVENTS_10503_TRACE_NAME_CONTEXT_FOREVER_LEVEL_2000);
        } finally {
            if (stmt != null && !stmt.isClosed()) {
                stmt.close();
            }
        }
    }

}
