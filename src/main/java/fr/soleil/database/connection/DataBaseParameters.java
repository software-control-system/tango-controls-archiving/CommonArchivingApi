package fr.soleil.database.connection;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.utils.GetConf;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Locale;

public class DataBaseParameters {

    private static final String POINT = ".";

    private DataBaseType dbType = DataBaseType.UNKNOWN;
    private String host;
    private String name;
    private String schema;
    private String user;
    private String password;

    private Object metricRegistry = null;
    private Object healthRegistry = null;
    private boolean harvestable = false;
    private short minPoolSize = DBConstants.DEFAULT_MIN_POOL_SIZE;
    private short maxPoolSize = DBConstants.DEFAULT_MAX_POOL_SIZE;
    private int inactivityTimeout = DBConstants.DEFAULT_INACTIVITY_TIMEOUT;


    private String jdbcURL = "";

    public DataBaseParameters() {
        super();
    }

    public DataBaseParameters(final DataBaseParameters params) {
        this.dbType = params.dbType;
        this.host = params.host;
        this.name = params.name;
        this.schema = params.schema;
        this.user = params.user;
        this.password = params.password;
        this.harvestable = params.harvestable;
        this.minPoolSize = params.minPoolSize;
        this.maxPoolSize = params.maxPoolSize;
        this.inactivityTimeout = params.inactivityTimeout;
        this.metricRegistry = params.metricRegistry;
        this.healthRegistry = params.healthRegistry;

    }


    public Object getMetricRegistry() {
        return metricRegistry;
    }

    public void setMetricRegistry(Object metricRegistry) {
        this.metricRegistry = metricRegistry;
    }

    public DataBaseType getDbType() {
        return dbType;
    }

    public void setDbType(final DataBaseType dbType) {
        this.dbType = dbType;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(final String schema) {
        this.schema = schema;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public boolean isHarvestable() {
        return harvestable;
    }

    public void setHarvestable(final boolean harvestable) {
        this.harvestable = harvestable;
    }

    public short getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(final short minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public short getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(final short maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getInactivityTimeout() {
        return inactivityTimeout;
    }

    public void setInactivityTimeout(final int inactivityTimeout) {
        this.inactivityTimeout = inactivityTimeout;
    }

    public void setJdbcURL(final String jdbcURL) {
        this.jdbcURL = jdbcURL;
    }

    public void setParametersFromTango(final String deviceClass) throws DevFailed {
        final String db = GetConf.readStringInDB(deviceClass, GetConf.DATABASE_TYPE_PROPERTY);
        dbType = DataBaseType.parseDataBaseType(db);
        host = GetConf.readStringInDB(deviceClass, GetConf.HOST_PROPERTY);
        name = GetConf.readStringInDB(deviceClass, GetConf.NAME_PROPERTY);
        user = GetConf.readStringInDB(deviceClass, GetConf.USER_PROPERTY);
        password = GetConf.readStringInDB(deviceClass, GetConf.PASSWORD_PROPERTY);
        // setOnsConf(GetConf.readStringInDB(deviceClass, GetConf.ONS_CONF_PROPERTY));
        // tnsNames = GetConf.readStringInDB(deviceClass, GetConf.TNS_NAMES_PROPERTY);
        // rac = GetConf.readBooleanObjectInDB(deviceClass, GetConf.IS_RAC_PROPERTY);
        schema = GetConf.readStringInDB(deviceClass, GetConf.SCHEMA_PROPERTY);
        minPoolSize = (short) getIntConf(deviceClass, GetConf.MIN_POOL_SIZE_PROPERTY,
                DBConstants.DEFAULT_MIN_POOL_SIZE);
        maxPoolSize = (short) getIntConf(deviceClass, GetConf.MAX_POOL_SIZE_PROPERTY,
                DBConstants.DEFAULT_MAX_POOL_SIZE);
        inactivityTimeout = getIntConf(deviceClass, GetConf.INACTIVITY_TIMEOUT_SIZE_PROPERTY,
                DBConstants.DEFAULT_INACTIVITY_TIMEOUT);
    }

    private int getIntConf(final String deviceClass, final String propName, final int defaultValue) throws DevFailed {
        int result = defaultValue;
        final String sreadConf = GetConf.readStringInDB(deviceClass, propName);
        try {
            result = Integer.parseInt(sreadConf);
        } catch (final NumberFormatException e) {
            // ignore
        }
        return result;
    }

    public String getUniqueKey() {
        final StringBuilder cm_url = new StringBuilder(getUrl());
        cm_url.append(POINT).append(user).append(POINT).append(schema);
        return cm_url.toString();
    }

    public String getUrl() {
        if (jdbcURL.isEmpty()) {
            final StringBuilder url = new StringBuilder();
            if (dbType.equals(DataBaseType.ORACLE)) {
                appendOracleUrltoStringBuilder(url);
            } else if (dbType.equals(DataBaseType.MYSQL)) {
                appendMySqlUrltoStringBuilder(url);
            } else if (dbType.equals(DataBaseType.H2)) {
                appendH2UrltoStringBuilder(url);
            } else if (dbType.equals(DataBaseType.POSTGRESQL)) {
                appendPostGreSqlUrltoStringBuilder(url);
            } else {
                appendMySqlUrltoStringBuilder(appendOracleUrltoStringBuilder(url).append(" then "));
            }
            return url.toString();
        } else {
            return jdbcURL;
        }
    }

    protected StringBuilder appendOracleUrltoStringBuilder(StringBuilder url) {
        url.append(DBConstants.DRIVER_ORACLE);
        url.append(":@");
        url.append(host).append(":").append(DBConstants.ORACLE_PORT).append(":").append(name);
        return url;
    }

    protected StringBuilder appendMySqlUrltoStringBuilder(StringBuilder url) {
        url.append(DBConstants.DRIVER_MYSQL).append("://").append(host).append("/").append(name);
        return url;
    }

    protected StringBuilder appendH2UrltoStringBuilder(StringBuilder url) {
        url.append("jdbc:h2:file:./target/h2db/db");
        return url;
    }

    protected StringBuilder appendPostGreSqlUrltoStringBuilder(StringBuilder url) {
        url.append(DBConstants.DRIVER_POSTGRESQL).append("://").append(host).append(DBConstants.POSTGRESQL_PORT).append("/").append(name);
        return url;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public Object getHealthRegistry() {
        return healthRegistry;
    }

    public void setHealthRegistry(Object healthRegistry) {
        this.healthRegistry = healthRegistry;
    }

    public enum DataBaseType {
        ORACLE, MYSQL, UNKNOWN, H2, POSTGRESQL;

        public static DataBaseType parseDataBaseType(final String value) {
            DataBaseType result = UNKNOWN;
            if (value != null) {
                result = valueOf(value.toUpperCase(Locale.ENGLISH));
            }
            return result;
        }
    }

}
