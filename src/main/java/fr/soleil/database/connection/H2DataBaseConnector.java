package fr.soleil.database.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class H2DataBaseConnector extends AbstractDataBaseConnector {

	protected static final String DRIVER_NOT_FOUND = "h2 driver not found ";


    public H2DataBaseConnector(final DataBaseParameters params) throws SQLException {
        super(params);
    }

    @Override
    public String getDriver() {
		return DBConstants.DRIVER_H2;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection conn;
		if (ds == null) {
            conn = null;
        } else {
			conn = ds.getConnection();
            if (USE_LOG4JDBC) {
                conn = new net.sf.log4jdbc.ConnectionSpy(conn);
            }
        }
        return conn;
    }


}
