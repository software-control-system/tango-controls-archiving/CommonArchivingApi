package fr.soleil.database.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class MySQLDataBaseConnector extends AbstractDataBaseConnector {

    protected static final String DRIVER_NOT_FOUND = "mysql driver not found ";


    public MySQLDataBaseConnector(final DataBaseParameters params) throws SQLException {
        super(params);
    }

    @Override
    public String getDriver() {
        return DBConstants.DRIVER_MYSQL;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection conn;
		if (ds == null) {
            conn = null;
        } else {
			conn = ds.getConnection();
            if (USE_LOG4JDBC) {
                conn = new net.sf.log4jdbc.ConnectionSpy(conn);
            }
        }
        return conn;
    }


}
