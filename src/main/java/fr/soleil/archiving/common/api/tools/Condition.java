//+======================================================================
// $Source:  $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  Condition.
//						(Garda Laure) - 1 juil. 2005
//
// $Author:  $
//
// $Revision: $
//
// $Log: $
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.common.api.tools;

import java.io.Serializable;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Description : A Condition object describes a search criterion for a request
 * into the database. A Condition contains : a name of a table's field an
 * operator a value.
 * 
 * @author GARDA
 */
public final class Condition implements Serializable {

    private static final long serialVersionUID = -7471356131547434572L;

    private String column; // Name of the table's field.
    private String operator; // Operator of the condition.
    private String value; // Value of the condition.

    /**
     * This constructor takes three parameters as inputs.
     * 
     * @param column
     * @param operator
     * @param value
     */
    public Condition(final String column, final String operator, final String value) {
        this.column = column;
        this.operator = operator;
        this.value = value;
    }

    /**
     * Returns the name of the table's field.
     * 
     * @return Name of the table's field
     */
    public String getColumn() {
        return column;
    }

    /**
     * Sets the name of the table's field.
     * 
     * @param column
     *            Name of the table's field
     */
    public void setColumn(final String column) {
        this.column = column;
    }

    /**
     * Returns the operator of the condition.
     * 
     * @return Operator of the condition
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Sets the operator of the condition.
     * 
     * @param operator
     *            Operator of the condition
     */
    public void setOperator(final String operator) {
        this.operator = operator;
    }

    /**
     * Returns the value of the condition.
     * 
     * @return Value of the condition
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the condition.
     * 
     * @param value
     *            Value of the condition
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Returns a String which represente the object Condition.
     * 
     * @return String which represente the object Condition
     */
    @Override
    public String toString() {
        return getColumn() + " " + getOperator() + " " + getValue() + "\r\n";
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Condition condition = (Condition) obj;
            equals = ObjectUtils.sameObject(column, condition.column)
                    && ObjectUtils.sameObject(operator, condition.operator)
                    && ObjectUtils.sameObject(value, condition.value);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xC02D;
        int mult = 0x17102;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(column);
        code = code * mult + ObjectUtils.getHashCode(operator);
        code = code * mult + ObjectUtils.getHashCode(value);
        return code;
    }

}
