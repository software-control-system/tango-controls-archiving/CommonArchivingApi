package fr.soleil.archiving.common.api.tools;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevError;
import fr.esrf.Tango.TimeVal;
import fr.esrf.TangoDs.TangoConst;
import fr.esrf.TangoDs.TimedAttrData;
import fr.soleil.database.DBExtractionConst;

//+======================================================================
// $Source: $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  DbData.
//						(chinkumo) - 31 ao�t 2005
//
// $Author: ounsy $
//O
// $Revision: 1.22 $
//
// $Log: DbData.java,v $
// Revision 1.22  2007/05/10 14:59:21  ounsy
// NaN and null distinction
//
// Revision 1.21  2007/02/28 09:46:37  ounsy
// better null values management
//
// Revision 1.20  2006/11/20 09:31:28  ounsy
// minor changes
//
// Revision 1.19  2006/11/09 14:19:57  ounsy
// addded a missing break in setData
//
// Revision 1.18  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.17  2006/08/29 13:56:36  ounsy
// setValue(XXX) : taking care of case where parameter is null
//
// Revision 1.16  2006/07/18 10:42:27  ounsy
// image support
//
// Revision 1.15  2006/05/05 14:07:50  ounsy
// corrected the "No Data" bug on write-only attributes
//
// Revision 1.14  2006/04/05 13:50:50  ounsy
// new types full support
//
// Revision 1.13  2006/03/27 15:20:01  ounsy
// new spectrum types support + better spectrum management + new scalar types full support
//
// Revision 1.12  2006/03/15 16:07:56  ounsy
// states converted to their string value only in GUI
//
// Revision 1.11  2006/03/15 15:11:13  ounsy
// minor changes
//
// Revision 1.10  2006/03/14 12:52:11  ounsy
// corrected the SNAP/spectrums/RW problem
// about the read and write values having the same length
//
// Revision 1.9  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.8  2006/02/16 10:37:01  ounsy
// code cleaning
//
// Revision 1.7  2006/02/16 09:57:27  ounsy
// No more confusion between table length and dim x
// splitDbData() allows read and write value to be of different sizes.
// splitDbData() [0] allways corresponds to the read value
// splitDbData() [1] allways corresponds to the write value
//
// Revision 1.6  2006/02/15 14:54:49  ounsy
// indexOutOfBounds avoided
//
// Revision 1.5  2006/02/07 11:54:11  ounsy
// removed useless logs
//
// Revision 1.4  2006/02/06 12:42:20  ounsy
// -corrected a bug in the case of successive spectrums with different sizes
// -added RW spectrum support
// -adapted the spitDbData method for spectrum attributes
// -added a debug method "traceShort"
//
// Revision 1.3  2006/01/27 14:13:33  ounsy
// spectrum extraction allowed
//
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.2.3  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/26 08:30:08  chinkumo
// no message
//
// Revision 1.1.2.1  2005/09/09 08:21:24  chinkumo
// First commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

public class DbData implements Serializable, Cloneable, DBExtractionConst {

    private static final long serialVersionUID = -5172549721523373516L;

    protected String name;
    /**
     * Max size for spectrums and images
     */
    private int maxX;
    private int maxY;

    /**
     * Data type
     */
    protected int dataType;
    /**
     * Data format
     */
    protected int dataFormat;
    /**
     * Writable
     */
    protected int writable;

    /**
     * Data found in HDB/TDB
     */
    protected NullableTimedData[] timedData;

    public DbData(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(final int data_type) {
        this.dataType = data_type;
    }

    public int getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(final int data_format) {
        this.dataFormat = data_format;
    }

    public int getWritable() {
        return writable;
    }

    public void setWritable(final int writable) {
        this.writable = writable;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(final int max_x) {
        this.maxX = max_x;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(final int max_y) {
        this.maxY = max_y;
    }

    public NullableTimedData[] getTimedData() {
        return timedData;
    }

    public void setTimedData(final NullableTimedData... data) {
        timedData = data;
    }

    public int size() {
        return timedData != null ? timedData.length : 0;
    }

    /**
     * This method is used to cope with today attribute tango java limitations
     * (read/write attribute are not supported !!).
     *
     * @return A two DbData object array; the first DbData is for read values; the second one for write values.
     * @deprecated Extraction methods should directly return the {@link DbData} array and this method should no more be
     *             used.
     */
    @Deprecated
    public DbData[] splitDbData() {
        final DbData[] argout = new DbData[2];
        final DbData dbData_r = new DbData(name);
        dbData_r.setDataType(dataType);
        dbData_r.setDataFormat(dataFormat);
        dbData_r.setWritable(writable);
        dbData_r.setMaxX(maxX);
        dbData_r.setMaxY(maxY);

        final DbData dbData_w = new DbData(name);
        dbData_w.setDataType(dataType);
        dbData_w.setDataFormat(dataFormat);
        dbData_w.setWritable(writable);
        dbData_w.setMaxX(0);
        dbData_w.setMaxY(maxY);

        if (timedData == null) {
            dbData_r.timedData = null;
            dbData_w.timedData = null;
            dbData_r.maxX = 0;
            dbData_r.maxY = 0;
            dbData_w.maxX = 0;
            dbData_w.maxY = 0;
            argout[READ_INDEX] = dbData_r;
            argout[WRITE_INDEX] = dbData_w;
        } else {
            final NullableTimedData[] timedAttrData_r = new NullableTimedData[timedData.length];
            final NullableTimedData[] timedAttrData_w = new NullableTimedData[timedData.length];
            for (int i = 0; i < timedData.length; i++) {
                if (timedData[i] != null) {
                    final boolean[] nullElements = (boolean[]) timedData[i].getNullElements();
                    final boolean[] nullElementsRead;
                    final boolean[] nullElementsWrite;
                    final int length = dataFormat == AttrDataFormat._IMAGE ? timedData[i].getX() * timedData[i].getY()
                            : timedData[i].getX();
                    if (nullElements == null) {
                        nullElementsRead = null;
                        nullElementsWrite = null;
                    } else {
                        nullElementsRead = Arrays.copyOf(nullElements, length);
                        nullElementsWrite = Arrays.copyOfRange(nullElements, length, nullElements.length);
                    }

                    final Object value = timedData[i].getValue();
                    Object ptr_read = null;
                    Object ptr_write = null;

                    if (value != null) {
                        int dimWrite = Array.getLength(value) - length;
                        if (dataFormat != AttrDataFormat._IMAGE) {
                            if (dimWrite > dbData_w.getMaxX()) {
                                dbData_w.setMaxX(dimWrite);
                            }
                        }
                        if (dimWrite < 0) {
                            dimWrite = 0;
                        }

                        ptr_read = initPrimitiveArray(length);
                        ptr_write = initPrimitiveArray(dimWrite);

                        if (ptr_read != null && ptr_write != null) {
                            System.arraycopy(value, 0, ptr_read, 0, length);
                            System.arraycopy(value, length, ptr_write, 0, dimWrite);
                        }
                    }

                    timedAttrData_r[i] = initNullableTimedData(dataType, timedData[i].getTime(), timedData[i].getX(),
                            timedData[i].getY());
                    timedAttrData_w[i] = initNullableTimedData(dataType, timedData[i].getTime(), timedData[i].getX(),
                            timedData[i].getY());

                    if (writable == AttrWriteType._WRITE) {
                        timedAttrData_r[i].setValue(ptr_write, nullElementsWrite);
                        timedAttrData_w[i].setValue(ptr_read, nullElementsRead);
                    } else {
                        timedAttrData_r[i].setValue(ptr_read, nullElementsRead);
                        timedAttrData_w[i].setValue(ptr_write, nullElementsWrite);
                    }
                } // end if (timedData[i] != null)
            } // end for (int i = 0; i < timedData.length; i++)
            dbData_r.setTimedData(timedAttrData_r);
            dbData_w.setTimedData(timedAttrData_w);
            switch (writable) {
                case AttrWriteType._READ:
                    argout[READ_INDEX] = dbData_r;
                    argout[WRITE_INDEX] = null;
                    break;
                case AttrWriteType._WRITE:
                    argout[READ_INDEX] = null;
                    argout[WRITE_INDEX] = dbData_w;
                    break;
                default:
                    argout[READ_INDEX] = dbData_r;
                    argout[WRITE_INDEX] = dbData_w;
            }
        } // end if (timedData == null) ... else
        return argout;
    }

    public TimedAttrData[] getDataAsTimedAttrData() {
        TimedAttrData[] attrData = null;
        if (timedData == null) {
            return null;
        } else {
            attrData = new TimedAttrData[timedData.length];
        }
        for (int i = 0; i < timedData.length; i++) {
            if (timedData[i] == null) {
                attrData[i] = null;
            } else {
                final int sec = (int) (timedData[i].getTime() / 1000);
                final TimeVal timeVal = new TimeVal(sec, 0, 0);
                Object value = timedData[i].getValue();
                switch (dataType) {
                    case TangoConst.Tango_DEV_BOOLEAN:
                        attrData[i] = new TimedAttrData(value == null ? new boolean[0] : ((boolean[]) value).clone(),
                                timeVal);
                        break;
                    case TangoConst.Tango_DEV_SHORT:
                    case TangoConst.Tango_DEV_USHORT:
                    case TangoConst.Tango_DEV_UCHAR:
                        attrData[i] = new TimedAttrData(value == null ? new short[0] : ((short[]) value).clone(),
                                timeVal);
                        break;
                    case TangoConst.Tango_DEV_LONG:
                    case TangoConst.Tango_DEV_ULONG:
                        attrData[i] = new TimedAttrData(value == null ? new int[0] : ((int[]) value).clone(), timeVal);
                        break;
                    case TangoConst.Tango_DEV_LONG64:
                    case TangoConst.Tango_DEV_ULONG64:
                        attrData[i] = new TimedAttrData(value == null ? new long[0] : ((long[]) value).clone(),
                                timeVal);
                        break;
                    case TangoConst.Tango_DEV_FLOAT:
                        attrData[i] = new TimedAttrData(value == null ? new float[0] : ((float[]) value).clone(),
                                timeVal);
                        break;
                    case TangoConst.Tango_DEV_DOUBLE:
                        attrData[i] = new TimedAttrData(value == null ? new double[0] : ((double[]) value).clone(),
                                timeVal);
                        break;
                    case TangoConst.Tango_DEV_STRING:
                        String[] strval;
                        if (timedData[i].getValue() == null) {
                            strval = new String[0];
                        } else {
                            strval = Arrays.stream((String[]) value).map(s -> s == null ? String.valueOf(s) : s)
                                    .toArray(String[]::new);
                        }
                        attrData[i] = new TimedAttrData(strval, timeVal);
                        break;

                    case TangoConst.Tango_DEV_STATE:
                        String[] strstate;
                        if (timedData[i].getValue() == null) {
                            strstate = new String[0];
                        } else {
                            final int[] state = (int[]) timedData[i].getValue();
                            strstate = new String[state.length];
                            for (int j = 0; j < state.length; j++) {
                                strstate[j] = TangoConst.Tango_DevStateName[state[j]];
                            }
                        }
                        attrData[i] = new TimedAttrData(strstate, timeVal);
                        break;
                    default:
                        attrData[i] = new TimedAttrData(new DevError[0], timeVal);
                }
                attrData[i].qual = timedData[i].getQual();
                attrData[i].x = timedData[i].getX();
                attrData[i].y = timedData[i].getY();
                attrData[i].data_type = dataType;
            }
        }
        return attrData;
    }

    @Override
    public String toString() {
        final ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this,
                ToStringStyle.MULTI_LINE_STYLE);
        return reflectionToStringBuilder.toString();
    }

    @Override
    protected DbData clone() {
        DbData clone;
        try {
            clone = (DbData) super.clone();
        } catch (final CloneNotSupportedException e) {
            // Should not happen as DbData is cloneable
            clone = new DbData(getName());
            clone.maxX = maxX;
            clone.maxY = maxY;
            clone.dataType = dataType;
            clone.dataFormat = dataFormat;
            clone.writable = writable;
        }
        if (timedData != null) {
            clone.timedData = timedData.clone();
            for (int i = 0; i < clone.timedData.length; i++) {
                if (clone.timedData[i] != null) {
                    clone.timedData[i] = clone.timedData[i].clone();
                }
            }
        }
        return clone;
    }

    public Object initPrimitiveArray(int length) {
        return initPrimitiveArray(getDataType(), length);
    }

    public void clean() {
        name = null;
        NullableTimedData[] timedDataArray = timedData;
        if (timedDataArray != null) {
            int i = 0;
            for (NullableTimedData timedData : timedDataArray) {
                if (timedData != null) {
                    timedData.clean();
                }
                timedDataArray[i++] = null;
            }
        }
        timedData = null;
    }

    @Override
    protected void finalize() {
        clean();
    }

    public static Object initPrimitiveArray(int dataType, int length) {
        switch (dataType) {
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_UCHAR:
                return new short[length];
            case TangoConst.Tango_DEV_DOUBLE:
                return new double[length];
            case TangoConst.Tango_DEV_FLOAT:
                return new float[length];
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG:
                return new int[length];
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
                return new long[length];
            case TangoConst.Tango_DEV_BOOLEAN:
                return new boolean[length];
            case TangoConst.Tango_DEV_STRING:
                return new String[length];
            default:
                return null;
        }
    }

    public static NullableTimedData initNullableTimedData(int dataType, long time, int dimX) {
        return initNullableTimedData(dataType, time, dimX, 0);
    }

    public static NullableTimedData initNullableTimedData(int dataType, long time) {
        return initNullableTimedData(dataType, time, 0, 0);
    }

    public static NullableTimedData initNullableTimedData(int dataType, long time, int dimX, int dimY) {
        NullableTimedData value = new NullableTimedData();
        value.setDataType(dataType);
        value.setX(dimX);
        value.setY(dimY);
        value.setTime(time);
        return value;
    }

    /**
     * Creates a standard {@link DbData} array that can be used to return db extraction data
     * 
     * @param attributeName The name of the attribute for which data will be extracted.
     * @param tfw The attribute information (data type, data format, writable).
     * @return A {@link DbData} array of length 2.
     */
    public static final DbData[] initExtractionResult(String attributeName, int... tfw) {
        DbData[] result = new DbData[2];
        if ((tfw != null) && (tfw.length > 2)) {
            DbData data = new DbData(attributeName);
            data.setDataType(tfw[0]);
            data.setDataFormat(tfw[1]);
            data.setWritable(tfw[2]);
            switch (tfw[2]) {
                case AttrWriteType._READ:
                    result[READ_INDEX] = data;
                    break;
                case AttrWriteType._WRITE:
                    result[WRITE_INDEX] = data;
                    break;
                default:
                    result[READ_INDEX] = data;
                    DbData writeData = new DbData(attributeName);
                    writeData.setDataType(tfw[0]);
                    writeData.setDataFormat(tfw[1]);
                    writeData.setWritable(tfw[2]);
                    result[WRITE_INDEX] = writeData;
                    break;
            }
        }
        return result;
    }

    public static DbData getFirstDbData(DbData... array) {
        DbData result = null;
        if (array != null) {
            for (DbData data : array) {
                if (data != null) {
                    result = data;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Fills {@link DbData} with extracted {@link NullableTimedData}.
     * 
     * @param readList The {@link NullableTimedData} list for the read part.
     * @param writeList The {@link NullableTimedData} list for the write part.
     * @param dbData The {@link DbData} array, previously prepared for extraction.
     */
    public static void afterExtraction(List<NullableTimedData> readList, List<NullableTimedData> writeList,
            DbData... dbData) {
        if (dbData[READ_INDEX] != null) {
            dbData[READ_INDEX].setTimedData(readList.toArray(new NullableTimedData[readList.size()]));
        }
        readList.clear();
        if (dbData[WRITE_INDEX] != null) {
            dbData[WRITE_INDEX].setTimedData(writeList.toArray(new NullableTimedData[writeList.size()]));
        }
        writeList.clear();
    }

}
