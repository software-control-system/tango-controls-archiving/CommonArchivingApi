package fr.soleil.archiving.common.api.utils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DbDevice;

public class DbConnectionInfo {

    public static final String ORIGIN_CLASS_PROPERTIES = "Class Properties";
    public static final String ORIGIN_SYSTEM_PROPERTIES = "System Properties";
    public static final String ORIGIN_DEVICE_PROPERTIES = "Device Properties";
    public static final String ORIGIN_ARGIN = "Arguments";
    public static final String ORIGIN_DEFAULT = "Default";

    // Types
    public static final String DB_DRIVER = "DB_DRIVER";
    public static final String DB_USER = "User";
    public static final String DB_PASSWORD = "Password";
    public static final String DB_HOST = "Host";
    public static final String DB_NAME = "Name";
    public static final String DB_SCHEMA = "Schema";
    public static final String DB_RAC = "Is Rac";
    public static final String DB_TNS = "DB_TNS";
    public static final String DB_ONS = "DB_ONS";
    public static final String DB_AUTO_COMMIT = "Auto commit";
    public static final String DB_HARVESTABLE = "Harvestable";

    private final DbInfoLogger dbUser = new DbInfoLogger(DB_USER);
    private final DbInfoLogger dbPassword = new DbInfoLogger(DB_PASSWORD);
    private final DbInfoLogger dbHost = new DbInfoLogger(DB_HOST);
    private final DbInfoLogger dbName = new DbInfoLogger(DB_NAME);
    private final DbInfoLogger dbSchema = new DbInfoLogger(DB_SCHEMA);
    private final DbInfoLogger dbRac = new DbInfoLogger(DB_RAC);

    private final String className;

    public DbConnectionInfo(String deviceClass) {
        className = deviceClass;
    }

    public String getDbUser() {
        return dbUser.getDbInfoItem();
    }

    public void setDbUser(String user, String origin) {
        dbUser.setInfo(user, origin);
    }

    public String getDbPassword() {
        return dbPassword.getDbInfoItem();
    }

    public void setDbPassword(String password, String origin) {
        dbPassword.setInfo(password, origin);
    }

    public String getDbHost() {
        return dbHost.getDbInfoItem();
    }

    public void setDbHost(String host, String origin) {
        dbHost.setInfo(host, origin);
    }

    public String getDbName() {
        return dbName.getDbInfoItem();
    }

    public void setDbName(String name, String origin) {
        dbName.setInfo(name, origin);
    }

    public String getDbSchema() {
        return dbSchema.getDbInfoItem();
    }

    public void setDbSchema(String schema, String origin) {
        dbSchema.setInfo(schema, origin);
    }

    public String isDbRac() {
        return dbRac.getDbInfoItem();
    }

    public void setDbRac(String rac, String origin) {
        dbRac.setInfo(rac, origin);
    }

    // /////////////////////////
    // Init Attributes
    // Goals to set a value only
    // if its empty
    // /////////////////////////

    private boolean isFillable(String original, String newValue) {
        return (DbInfoLogger.isEmpty(original) && newValue != null && !newValue.isEmpty());
    }

    public void fillDbUser(String dbUser, String origin) {
        if (isFillable(getDbUser(), dbUser)) {
            setDbUser(dbUser, origin);
        }
    }

    public void fillDbPassword(String dbPassword, String origin) {
        if (isFillable(getDbPassword(), dbPassword)) {
            setDbPassword(dbPassword, origin);
        }
    }

    public void fillDbHost(String dbHost, String origin) {
        if (isFillable(getDbHost(), dbHost)) {
            setDbHost(dbHost, origin);
        }
    }

    public void fillDbName(String dbName, String origin) {
        if (isFillable(getDbName(), dbName)) {
            setDbName(dbName, origin);
        }
    }

    public void fillDbSchema(String dbSchema, String origin) {
        if (isFillable(getDbSchema(), dbSchema)) {
            setDbSchema(dbSchema, origin);
        }
    }

    public void fillDbRac(String dbRac, String origin) {
        if (isFillable(isDbRac(), dbRac)) {
            setDbRac(dbRac, origin);
        }
    }

    // ////////////////////
    // Init with properties
    // ////////////////////

    public void initFromDeviceProperties(final DbDevice device) throws DevFailed {
        String[] propnames = { "DbHost", "DbName", "DbSchema", "DbUser", "DbPassword", "IsRac" };
        initFromDevicePropertiesList(device, propnames);
    }

    public void initFromDevicePropertiesList(final DbDevice device, String[] propnames) throws DevFailed {

        DbDatum[] dev_prop = device.get_property(propnames);

        if (dev_prop[0].is_empty() == false) {
            fillDbHost(dev_prop[0].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
        if (dev_prop[1].is_empty() == false) {
            fillDbName(dev_prop[1].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
        if (dev_prop[2].is_empty() == false) {
            fillDbSchema(dev_prop[2].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
        if (dev_prop[3].is_empty() == false) {
            fillDbUser(dev_prop[3].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
        if (dev_prop[4].is_empty() == false) {
            fillDbPassword(dev_prop[4].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
        if (dev_prop[5].is_empty() == false) {
            fillDbRac(dev_prop[5].extractString(), ORIGIN_DEVICE_PROPERTIES);
        }
    }

    public void initFromClassProperties() throws DevFailed {
        fillDbHost(GetConf.getHost(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
        fillDbName(GetConf.getName(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
        fillDbSchema(GetConf.getSchema(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
        fillDbUser(GetConf.getUser(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
        fillDbPassword(GetConf.getPwd(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
        fillDbRac(GetConf.isRAC(className), ORIGIN_CLASS_PROPERTIES + " (" + className + ")");
    }

    public void initFromDefaultProperties(final String default_host, final String default_name,
            final String default_schema, final String default_user, final String default_pass,
            final String default_isRac) {
        fillDbHost(default_host, ORIGIN_DEFAULT);
        fillDbName(default_name, ORIGIN_DEFAULT);
        fillDbSchema(default_schema, ORIGIN_DEFAULT);
        fillDbUser(default_user, ORIGIN_DEFAULT);
        fillDbPassword(default_pass, ORIGIN_DEFAULT);
        fillDbRac(default_isRac, ORIGIN_DEFAULT);
    }

    public void initFromSystemProperties(final String host_property, final String name_property,
            final String schema_property, final String user_property, final String pass_property,
            final String isRac_property) {
        fillDbHost(System.getProperty(host_property, getDbHost()), ORIGIN_SYSTEM_PROPERTIES);
        fillDbName(System.getProperty(name_property, getDbName()), ORIGIN_SYSTEM_PROPERTIES);
        fillDbSchema(System.getProperty(schema_property, getDbSchema()), ORIGIN_SYSTEM_PROPERTIES);
        fillDbUser(System.getProperty(user_property, getDbUser()), ORIGIN_SYSTEM_PROPERTIES);
        fillDbPassword(System.getProperty(pass_property, getDbPassword()), ORIGIN_SYSTEM_PROPERTIES);
        fillDbRac(System.getProperty(isRac_property, isDbRac()), ORIGIN_SYSTEM_PROPERTIES);
    }

    public void initFromArgumentsProperties(final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) {
        fillDbHost(host, ORIGIN_ARGIN);
        fillDbName(name, ORIGIN_ARGIN);
        fillDbSchema(schema, ORIGIN_ARGIN);
        fillDbUser(user, ORIGIN_ARGIN);
        fillDbPassword(pass, ORIGIN_ARGIN);
        fillDbRac(isRac, ORIGIN_ARGIN);
    }

    // //////////
    // Print logs
    // //////////

    public StringBuilder appendLoggerTrace(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        dbHost.appendLoggerTrace(result);
        dbName.appendLoggerTrace(result.append('\n'));
        dbSchema.appendLoggerTrace(result.append('\n'));
        dbUser.appendLoggerTrace(result.append('\n'));
        dbPassword.appendLoggerTrace(result.append('\n'));
        dbRac.appendLoggerTrace(result.append('\n'));
        return result;
    }

}
