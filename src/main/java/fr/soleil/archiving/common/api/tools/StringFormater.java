/*
 * Synchrotron Soleil
 * 
 * File : StringFormater.java
 * 
 * Project : apiDev
 * 
 * Description :
 * 
 * Author : SOLEIL
 * 
 * Original : 28 f�vr. 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: StringFormater.java,v
 */
package fr.soleil.archiving.common.api.tools;

public final class StringFormater {

    private static final String EMPTY_STRING_REPLACEMENT = " ";
    private static final String EMPTY_STRING = "";

    // Special characters
    private static final String[] PROTECT = { "[", "]", ",", ";", "'", "\"", "~", "\n" };

    // Associated code : in fact, the corresponding xml code number (ISO 8859/1)
    private static final String[] CODE = { "91", "93", "44", "59", "39", "34", "126", "10" };

    // transforms #code# -> corresponding character
    public static String formatStringToRead(final String toFormat) {
        String result;
        if (toFormat == null) {
            result = EMPTY_STRING;
        } else {
            result = toFormat;
            for (int i = 0; i < CODE.length; i++) {
                result = result.replace("#" + CODE[i] + "#", PROTECT[i]);
            }
        }
        return result;
    }

    // transforms character -> corresponding #code#
    public static String formatStringToWrite(final String toFormat) {
        String result;
        if (toFormat == null) {
            result = EMPTY_STRING;
        } else if (toFormat.isEmpty()) {
            result = EMPTY_STRING_REPLACEMENT;
        } else {
            result = toFormat;
            for (int i = 0; i < PROTECT.length; i++) {
                result = result.replace(PROTECT[i], "#" + CODE[i] + "#");
            }
        }
        return result;
    }
}
