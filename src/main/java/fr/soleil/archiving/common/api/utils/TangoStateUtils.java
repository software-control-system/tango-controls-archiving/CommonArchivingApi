package fr.soleil.archiving.common.api.utils;

import java.util.Set;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoDs.DeviceImpl;
import fr.esrf.TangoDs.TangoConst;

public final class TangoStateUtils {
    private TangoStateUtils() {

    }

    public static void setFault(final DeviceImpl device, final String msg) {
        device.set_state(DevState.FAULT);
        device.set_status(msg);
    }

    public static void setRunning(final DeviceImpl device) {
        device.set_state(DevState.RUNNING);
        device.set_status("Starting archiving");
    }

    public static void updateState(final DeviceImpl device, final Set<String> koAttributes,
            final Set<String> okAttributes) {
        if (koAttributes.isEmpty() && okAttributes.isEmpty()) {
            device.set_state(DevState.OFF);
            device.set_status("No attributes to archive");
        } else if (!koAttributes.isEmpty()) {
            device.set_state(DevState.DISABLE);
            device.set_status(statusToString(koAttributes, okAttributes));
        } else {
            device.set_state(DevState.ON);
            device.set_status(statusToString(koAttributes, okAttributes));
        }
    }

    public static void isAllowed(final DeviceImpl device) throws DevFailed {
        if (device.get_state().equals(DevState.FAULT) || device.get_state().equals(DevState.RUNNING)) {
            DevError err = new DevError();
            err.desc = "action is not allowed while device is "
                    + TangoConst.Tango_DevStateName[device.get_state().value()];
            err.severity = ErrSeverity.ERR;
            err.reason = "NOT_ALLOWED";
            err.origin = "isAllowed";

            throw new DevFailed(new DevError[] { err });
        }
    }

    public static String statusToString(final Set<String> koAttributes, final Set<String> okAttributes) {
        final StringBuilder sb = new StringBuilder("- KO [");
        for (final String koS : koAttributes) {
            sb.append("\n").append(koS);
        }
        sb.append("]\n- OK [");
        for (final String okS : okAttributes) {
            sb.append("\n").append(okS);
        }
        sb.append("]");
        return sb.toString();
    }
}
