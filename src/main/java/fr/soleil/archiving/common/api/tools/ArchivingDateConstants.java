package fr.soleil.archiving.common.api.tools;

import java.time.format.DateTimeFormatter;

public interface ArchivingDateConstants {

    public static final String EU_SECONDS_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String US_SECONDS_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final DateTimeFormatter EU_FORMAT = DateTimeFormatter.ofPattern(EU_SECONDS_DATE_FORMAT);
    public static final DateTimeFormatter US_FORMAT = DateTimeFormatter.ofPattern(US_SECONDS_DATE_FORMAT);

}
