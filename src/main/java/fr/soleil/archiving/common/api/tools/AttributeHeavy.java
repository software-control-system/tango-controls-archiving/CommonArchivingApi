//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/AttributeHeavy.java,v $
//
// Project:      Tango Archiving Service
//
// Description: The AttributeHeavy object describes a TANGO attribute about to be registered into the filing service.
//				It thus contains all available informations characterizing it.
//				An AttributeHeavy contains :
//				- a registration time,
//				- a device name (The device to which it belongs to),
//				- a domain (The domain to which it is associated),
//				- a family (The family to which it is associated),
//				- a member (The member to which it is associated),
//				- a name (its personal name, without the device part),
//				- an attribute type property (DevShort/DevLong/DevDouble),
//				- an attribute data format property (SCALAR/SPECTRUM/IMAGE),
//				- an attribute writable property (READ/READ_WRITE/READ_WITH_WRITE),
//				- the max_dim_x parameter (maximum size for attributes of the SPECTRUM and IMAGE data format)
//				- the max_dim_y parameter (maximum size for attributes of the IMAGE data format)
//				- a level parameter (This parameter is only an help for graphical application - this parameter defines an operator mode or an expert mode).
//				- a control system reference (Control system to which belongs the attribute)
//				- an archivable property that precises whether the attribute is "on-run-only" archivable, or if it is "always" archivable.
//				- a substitute parameter (Name of the attribute eventualy replaced by this one)
//				- a description (Attribute description)
//				- a label (Attribute label)
//				- a unit (Attribute unit)
//				- a standard_unit (Conversion factor to MKSA unit)
//				- a display_unit (The attribute unit in a printable form)
//				- a format (How to print attribute value)
//				- a min_value (Attribute min value)
//				- a max_value (Attribute max value)
//				- a min_alarm (Attribute low level alarm)
//				- a max_alarm (Attribute high level alarm)
//				- some optional_properties
//				- a target (TANGO device in charge of inserting datum into the database)
//				- a collector (TANGO device in charge of collecting events)
//				- a mode of filling (Constraints applied to the filing).
//
//
// $Author: chinkumo $
//
// $Revision: 1.4 $
//
// $Log: AttributeHeavy.java,v $
// Revision 1.4  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.3.12.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.3  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.2.4.1  2005/04/29 18:35:03  chinkumo
// Changes made to remove deprecated objetcs (ListeAttributs, ListeDevices, Device)
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.common.api.tools;

/**
 * <p/>
 * <B>Description :</B><BR>
 * The <I>AttributeHeavy</I> object describes a TANGO attribute about to be
 * registered into the filing service. It thus contains all available
 * informations characterizing it. An <I>AttributeHeavy</I> contains :
 * <ul>
 * <li>a <I>registration time</I>,
 * <li>a <I>device name</I> (The device to which it belongs to),
 * <li>a <I>domain</I> (The domain to which it is associated),
 * <li>a <I>family</I> (The family to which it is associated),
 * <li>a <I>member</I> (The member to which it is associated),
 * <li>a <I>name</I> (its personal name, without the device part),
 * <li>an <I>attribute type property</I> (DevShort/DevLong/DevDouble),
 * <li>an <I>attribute data format property</I> (SCALAR/SPECTRUM/IMAGE),
 * <li>an <I>attribute writable property</I> (READ/READ_WRITE/READ_WITH_WRITE),
 * <li>the <I>max_dim_x parameter</I> (maximum size for attributes of the
 * SPECTRUM and IMAGE data format)
 * <li>the <I>max_dim_y parameter</I> (maximum size for attributes of the IMAGE
 * data format)
 * <li>a <I>level parameter</I> (This parameter is only an help for graphical
 * application - this parameter defines an operator mode or an expert mode).
 * <li>a <I>control system reference</I> (Control system to which belongs the
 * attribute)
 * <li>an <I>archivable property</I> that precises whether the attribute is
 * "on-run-only" archivable, or if it is "always" archivable.
 * <li>a <I>substitute parameter</I> (Name of the attribute eventualy replaced
 * by this one)
 * <p/>
 * <li>a <I>description</I> (Attribute description)
 * <li>a <I>label</I> (Attribute label)
 * <li>a <I>unit</I> (Attribute unit)
 * <li>a <I>standard_unit</I> (Conversion factor to MKSA unit)
 * <li>a <I>display_unit</I> (The attribute unit in a printable form)
 * <li>a <I>format</I> (How to print attribute value)
 * <li>a <I>min_value</I> (Attribute min value)
 * <li>a <I>max_value</I> (Attribute max value)
 * <li>a <I>min_alarm</I> (Attribute low level alarm)
 * <li>a <I>max_alarm</I> (Attribute high level alarm)
 * <li>some <I>optional_properties</I>
 * <p/>
 * <li>a <I>target</I> (TANGO device in charge of inserting datum into the
 * database)
 * <li>a <I>collector</I> (TANGO device in charge of collecting events)
 * <li>a <I>mode of filling</I> (Constraints applied to the filing).
 * </ul>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.4 $
 * @see fr.soleil.archiving.common.api.tools.ArchivingEvent
 */
public final class AttributeHeavy extends AttributeLight {
    private java.sql.Timestamp registrationTime = null;
    private String attributeDeviceName = "";

    private String domain = "";
    private String family = "";
    private String member = "";

    private String attributeName = "";

    private int maxDimX = 0;
    private int maxDimY = 0;
    private final int level = 0;
    private String ctrlSys = "";
    private int archivable = 0;
    private int substitute = 0;

    private String description = ""; // **************** DESCRIPTION
    private String label = ""; // **************** LABEL
    private String unit = ""; // **************** UNIT
    private String standardUnit = "";// **************** STANDARD_UNIT
    private String displayUnit = ""; // **************** DISPLAY_UNIT
    private String format = ""; // **************** FORMAT
    private String minValue = ""; // **************** MIN_VALUE
    private String maxValue = ""; // **************** MAX_VALUE
    private String minAlarm = ""; // **************** MIN_ALARM
    private String maxAlarm = ""; // **************** MAX_ALARM
    private String optionalProperties = ""; // ****************

    // OPTIONAL_PROPERTIES

    /**
     * Default constructor Creates a new instance of AttributeHeavy
     * 
     * @see #AttributeHeavy(String)
     * @see #AttributeHeavy(String[])
     * @see AttributeLight
     */
    public AttributeHeavy() {
    }

    /**
     * This constructor takes one parameter as inputs.
     * 
     * @param n
     *            the attribute's full name (device part + attribut part)
     * @see #AttributeHeavy(String[])
     * @see AttributeLight
     */
    public AttributeHeavy(final String n) {
        super(n);
    }


    /**
     * Returns the AttributeHeavy's <I>registration time</I>.
     * 
     * @return the AttributeHeavy's <I>registration time</I>..
     * @see #setRegistration_time
     * @see #getAttribute_complete_name
     */
    public java.sql.Timestamp getRegistration_time() {
        return this.registrationTime;
    }

    /**
     * Sets the AttributeHeavy's <I>registration time</I>.
     * 
     * @param registration_time
     *            the AttributeHeavy's <I>registration time</I>.
     */
    public void setRegistration_time(final java.sql.Timestamp registrationTime) {
        this.registrationTime = registrationTime;
    }


    /**
     * Returns the AttributeHeavy's <I>device name</I> (The device to which it
     * belongs to).
     * 
     * @return the AttributeHeavy's <I>device name</I> (The device to which it
     *         belongs to).
     */
    public String getAttribute_device_name() {
        return this.attributeDeviceName;
    }

    /**
     * Sets the AttributeHeavy's <I>device name</I> (The device to which it
     * belongs to).
     * 
     * @param attribute_device_name
     *            the AttributeHeavy's <I>device name</I> (The device to which
     *            it belongs to).
     */
    public void setAttribute_device_name(final String attributeDeviceName) {
        this.attributeDeviceName = attributeDeviceName;
    }

    /**
     * Returns the AttributeHeavy's <I>domain</I> (The domain to which it is
     * associated) .
     * 
     * @return the AttributeHeavy's <I>domain</I> (The domain to which it is
     *         associated).
     */
    public String getDomain() {
        return this.domain;
    }

    /**
     * Sets the AttributeHeavy's <I>domain</I> (The domain to which it is
     * associated) .
     * 
     * @param domain
     *            the AttributeHeavy's <I>domain</I> (The domain to which it is
     *            associated) .
     */
    public void setDomain(final String domain) {
        this.domain = domain;
    }

    /**
     * Returns the AttributeHeavy's <I>family</I> (The family to which it is
     * associated)
     * 
     * @return the AttributeHeavy's <I>family</I> (The family to which it is
     *         associated)
     */
    public String getFamily() {
        return family;
    }

    /**
     * Sets the <I>family</I> (The family to which it is associated)
     * 
     * @param family
     *            the <I>family</I> (The family to which it is associated)
     */
    public void setFamily(final String family) {
        this.family = family;
    }

    /**
     * Returns the AttributeHeavy's <I>member</I> (The member to which it is
     * associated).
     * 
     * @return the AttributeHeavy's <I>member</I> (The member to which it is
     *         associated) .
     */
    public String getMember() {
        return member;
    }

    /**
     * Sets the AttributeHeavy's <I>member</I> (The member to which it is
     * associated)
     * 
     * @param member
     *            the AttributeHeavy's <I>member</I> (The member to which it is
     *            associated)
     */
    public void setMember(final String member) {
        this.member = member;
    }

    /**
     * Returns the AttributeHeavy's <I>name</I> (its personal name, without the
     * device part).
     * 
     * @return the AttributeHeavy's <I>name</I> (its personal name, without the
     *         device part)
     */
    public String getAttribute_name() {
        return attributeName;
    }

    /**
     * Sets the AttributeHeavy's <I>name</I> (its personal name, without the
     * device part)
     * 
     * @param attribute_name
     *            the AttributeHeavy's <I>name</I> (its personal name, without
     *            the device part)
     */
    public void setAttribute_name(final String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * Returns the AttributeHeavy's <I>max_dim_x parameter</I> (maximum size for
     * attributes of the SPECTRUM and IMAGE data format).
     * 
     * @return the AttributeHeavy's <I>max_dim_x parameter</I> (maximum size for
     *         attributes of the SPECTRUM and IMAGE data format).
     */
    public int getMax_dim_x() {
        return this.maxDimX;
    }

    /**
     * Sets the AttributeHeavy's <I>max_dim_x parameter</I> (maximum size for
     * attributes of the SPECTRUM and IMAGE data format).
     * 
     * @param max_dim_x
     *            the AttributeHeavy's <I>max_dim_x parameter</I> (maximum size
     *            for attributes of the SPECTRUM and IMAGE data format).
     */
    public void setMax_dim_x(final int maxDimX) {
        this.maxDimX = maxDimX;
    }

    /**
     * Returns the AttributeHeavy's <I>max_dim_y parameter</I> (maximum size for
     * attributes of the SPECTRUM and IMAGE data format).
     * 
     * @return the AttributeHeavy's <I>max_dim_y parameter</I> (maximum size for
     *         attributes of the SPECTRUM and IMAGE data format).
     */
    public int getMax_dim_y() {
        return this.maxDimY;
    }

    /**
     * Sets the AttributeHeavy's <I>max_dim_y parameter</I> (maximum size for
     * attributes of the SPECTRUM and IMAGE data format).
     * 
     * @param max_dim_y
     *            the AttributeHeavy's <I>max_dim_y parameter</I> (maximum size
     *            for attributes of the SPECTRUM and IMAGE data format).
     */
    public void setMax_dim_y(final int maxDimY) {
        this.maxDimY = maxDimY;
    }

    /**
     * Returns the AttributeHeavy's <I>level parameter</I> (This parameter is
     * only an help for graphical application - this parameter defines an
     * operator mode or an expert mode).
     * 
     * @return the AttributeHeavy's <I>level parameter</I> (This parameter is
     *         only an help for graphical application - this parameter defines
     *         an operator mode or an expert mode).
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * Sets the <I>level parameter</I> (This parameter is only an help for
     * graphical application - this parameter defines an operator mode or an
     * expert mode).
     * 
     * @param level
     *            the <I>level parameter</I> (This parameter is only an help for
     *            graphical application - this parameter defines an operator
     *            mode or an expert mode).
     */
    public void setLevel(int level) {
        level = this.level;
    }

    /**
     * Returns the AttributeHeavy's <I>Control system info</I> (Control system
     * to which belongs the attribute).
     * 
     * @return the AttributeHeavy's <I>Control system info</I> (Control system
     *         to which belongs the attribute).
     */
    public String getCtrl_sys() {
        return this.ctrlSys;
    }

    /**
     * Sets the AttributeHeavy's <I>Control system info</I> (Control system to
     * which belongs the attribute).
     * 
     * @param ctrl_sys
     *            the AttributeHeavy's <I>Control system info</I> (Control
     *            system to which belongs the attribute).
     */
    public void setCtrl_sys(final String ctrlSys) {
        this.ctrlSys = ctrlSys;
    }

    /**
     * Returns the AttributeHeavy's <I>archivable property</I> that precises
     * whether the attribute is "on-run-only" archivable or if it is "always"
     * archivable.
     * 
     * @return the AttributeHeavy's <I>archivable property</I> that precises
     *         whether the attribute is "on-run-only" archivable or if it is
     *         "always" archivable.
     */
    public int getArchivable() {
        return this.archivable;
    }

    /**
     * Sets the AttributeHeavy's <I>archivable property</I> that precises
     * whether the attribute is "on-run-only" archivable or if it is "always"
     * archivable.
     * 
     * @param archivable
     *            the AttributeHeavy's <I>archivable property</I> that precises
     *            whether the attribute is "on-run-only" archivable or if it is
     *            "always" archivable.
     */
    public void setArchivable(final int archivable) {
        this.archivable = archivable;
    }

    /**
     * Returns the AttributeHeavy's <I>substitute parameter</I> (Name of the
     * attribute eventualy replaced by this one).
     * 
     * @return the AttributeHeavy's <I>substitute parameter</I> (Name of the
     *         attribute eventualy replaced by this one).
     */
    public int getSubstitute() {
        return this.substitute;
    }

    /**
     * Sets the AttributeHeavy's <I>substitute parameter</I> (Name of the
     * attribute eventualy replaced by this one).
     * 
     * @param substitute
     *            the AttributeHeavy's <I>substitute parameter</I> (Name of the
     *            attribute eventualy replaced by this one).
     */
    public void setSubstitute(final int substitute) {
        this.substitute = substitute;
    }

    /**
     * Returns the AttributeHeavy's <I>description</I> (Attribute description).
     * 
     * @return the AttributeHeavy's <I>description</I> (Attribute description).
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the AttributeHeavy's <I>description</I> (Attribute description).
     * 
     * @param description
     *            the AttributeHeavy's <I>description</I> (Attribute
     *            description).
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Returns the AttributeHeavy's <I>label</I> (Attribute label).
     * 
     * @return the AttributeHeavy's <I>label</I> (Attribute label).
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Sets the AttributeHeavy's <I>label</I> (Attribute label).
     * 
     * @param label
     *            the AttributeHeavy's <I>label</I> (Attribute label).
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Returns the AttributeHeavy's <I>unit</I> (Attribute unit).
     * 
     * @return the AttributeHeavy's <I>unit</I> (Attribute unit).
     */
    public String getUnit() {
        return this.unit;
    }

    /**
     * Sets the AttributeHeavy's <I>unit</I> (Attribute unit).
     * 
     * @param unit
     *            the AttributeHeavy's <I>unit</I> (Attribute unit).
     */
    public void setUnit(final String unit) {
        this.unit = unit;
    }

    /**
     * Returns the AttributeHeavy's <I>standard_unit</I> (Conversion factor to
     * MKSA unit).
     * 
     * @return the AttributeHeavy's <I>standard_unit</I> (Conversion factor to
     *         MKSA unit).
     */
    public String getStandard_unit() {
        return this.standardUnit;
    }

    /**
     * Sets the AttributeHeavy's <I>standard_unit</I> (Conversion factor to MKSA
     * unit).
     * 
     * @param standard_unit
     *            the AttributeHeavy's <I>standard_unit</I> (Conversion factor
     *            to MKSA unit).
     */
    public void setStandard_unit(final String standardUnit) {
        this.standardUnit = standardUnit;
    }

    /**
     * Returns the AttributeHeavy's <I>display_unit</I> (The attribute unit in a
     * printable form).
     * 
     * @return the AttributeHeavy's <I>display_unit</I> (The attribute unit in a
     *         printable form).
     */
    public String getDisplay_unit() {
        return this.displayUnit;
    }

    /**
     * Sets the AttributeHeavy's <I>display_unit</I> (The attribute unit in a
     * printable form).
     * 
     * @param display_unit
     *            the AttributeHeavy's <I>display_unit</I> (The attribute unit
     *            in a printable form).
     */
    public void setDisplay_unit(final String displayUnit) {
        this.displayUnit = displayUnit;
    }

    /**
     * Returns the AttributeHeavy's <I>format</I> (How to print attribute
     * value).
     * 
     * @return the AttributeHeavy's <I>format</I> (How to print attribute
     *         value).
     */
    public String getFormat() {
        return this.format;
    }

    /**
     * Sets the AttributeHeavy's <I>format</I> (How to print attribute value).
     * 
     * @param format
     *            the AttributeHeavy's <I>format</I> (How to print attribute
     *            value).
     */
    public void setFormat(final String format) {
        this.format = format;
    }

    /**
     * Returns the AttributeHeavy's <I>min_value</I> (Attribute min value).
     * 
     * @return the AttributeHeavy's <I>min_value</I> (Attribute min value).
     */
    public String getMin_value() {
        return this.minValue;
    }

    /**
     * Sets the AttributeHeavy's <I>min_value</I> (Attribute min value).
     * 
     * @param min_value
     *            the AttributeHeavy's <I>min_value</I> (Attribute min value).
     */
    public void setMin_value(final String minValue) {
        this.minValue = minValue;
    }

    /**
     * Returns the AttributeHeavy's <I>max_value</I> (Attribute max value).
     * 
     * @return the AttributeHeavy's <I>max_value</I> (Attribute max value).
     */
    public String getMax_value() {
        return this.maxValue;
    }

    /**
     * Sets the AttributeHeavy's <I>max_value</I> (Attribute max value).
     * 
     * @param max_value
     *            the AttributeHeavy's <I>max_value</I> (Attribute max value).
     */
    public void setMax_value(final String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * Returns the AttributeHeavy's <I>min_alarm</I> (Attribute low level
     * alarm).
     * 
     * @return the AttributeHeavy's <I>min_alarm</I> (Attribute low level
     *         alarm).
     */
    public String getMin_alarm() {
        return this.minAlarm;
    }

    /**
     * Sets the AttributeHeavy's <I>min_alarm</I> (Attribute low level alarm).
     * 
     * @param min_alarm
     *            the AttributeHeavy's <I>min_alarm</I> (Attribute low level
     *            alarm).
     */
    public void setMin_alarm(final String minAlarm) {
        this.minAlarm = minAlarm;
    }

    /**
     * Returns the AttributeHeavy's <I>max_alarm</I> (Attribute high level
     * alarm).
     * 
     * @return the AttributeHeavy's <I>max_alarm</I> (Attribute high level
     *         alarm).
     */
    public String getMax_alarm() {
        return this.maxAlarm;
    }

    /**
     * Sets the AttributeHeavy's <I>max_alarm</I> (Attribute high level alarm)
     * 
     * @param max_alarm
     *            the AttributeHeavy's <I>max_alarm</I> (Attribute high level
     *            alarm)
     */
    public void setMax_alarm(final String maxAlarm) {
        this.maxAlarm = maxAlarm;
    }

    /**
     * Returns the AttributeHeavy's <I>optional_properties</I>.
     * 
     * @return the AttributeHeavy's <I>optional_properties</I>.
     */
    public String getOptional_properties() {
        return this.optionalProperties;
    }

    /**
     * Sets the AttributeHeavy's <I>optional_properties</I>
     * 
     * @param optional_properties
     *            the AttributeHeavy's <I>optional_properties</I>
     */
    public void setOptional_properties(final String optionalProperties) {
        this.optionalProperties = optionalProperties;
    }

    /**
     * Returns a string representation of the object <I>AttributeHeavy</I>.
     * 
     * @return a string representation of the object <I>AttributeHeavy</I>.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder("");
        buf.append("\tname ......................." + getAttributeCompleteName() + "\n");
        buf.append("\t**** Donn�es propres � l'ADT... **** \n");
        buf.append("\tRegistration Time .........." + getRegistration_time().toString() + "\n"); // ****************
		// Attribute registration timestamp
        buf.append("\tAttribute_complete_name ...." + getAttributeCompleteName() + "\n"); // ****************
		// The whole attribute name (device_name + // attribute_name)
        buf.append("\tDevice name ................" + getAttribute_device_name() + "\n"); // ****************
		// name // of // the // belonging // device.
        buf.append("\tDomain ....................." + getDomain() + "\n"); // ****************
        // domain
        // to
        // which
        // the
        // attribute
        // is
        // associated
        buf.append("\tFamily ....................." + getFamily() + "\n"); // ****************
        // family
        // to
        // which
        // the
        // attribute
        // is
        // associated
        buf.append("\tMember ....................." + getMember() + "\n"); // ****************
        // member
        // to
        // which
        // the
        // attribute
        // is
        // associated

        buf.append("\tAttribute name ............." + getAttribute_name() + "\n"); // ****************
        // attribute
        // name
        buf.append("\tData type property ........." + Integer.toString(getDataType()) + "\n"); // ****************
        // Attribute
        // data
        // type
        buf.append("\tData format property ......." + Integer.toString(getDataFormat()) + "\n"); // ****************
        // Attribute
        // data
        // format
        buf.append("\tWritable property .........." + Integer.toString(getWritable()) + "\n"); // ****************
        // Attribute
        // read/write
        // type
        buf.append("\tMaximum X dimension ........" + Integer.toString(getMax_dim_x()) + "\n"); // ****************
        // Attribute
        // Maximum
        // X
        // dimension
        buf.append("\tMaximum X dimension ........" + Integer.toString(getMax_dim_y()) + "\n"); // ****************
        // Attribute
        // Maximum
        // Y
        // dimension
        buf.append("\tDisplay level .............." + Integer.toString(getLevel()) + "\n"); // ****************
        // Attribute
        // display
        // level
        buf.append("\tCtrl system ................" + getCtrl_sys() + "\n"); // ****************
        // Control
        // system
        // to
        // which
        // the
        // attribute
        // belongs
        buf.append("\tArchivable  ................" + Integer.toString(getArchivable()) + "\n"); // ****************
        // archivable
        // (Property
        // that
        // precises
        // whether
        // the
        // attribute
        // is
        // "on-run-only"
        // archivable,
        // or
        // if
        // it
        // is
        // "always"
        // archivable
        buf.append("\tSubstitute ................." + Integer.toString(getSubstitute()) + "\n"); // ****************
        // substitute
        buf.append("\t**** Donn�es propres � l'APT... **** \n");
        buf.append("\tDescription ................" + getDescription() + "\n"); // ****************
        // DESCRIPTION
        buf.append("\tLabel ......................" + getLabel() + "\n"); // ****************
        // LABEL
        buf.append("\tUnit ......................." + getUnit() + "\n"); // ****************
        // UNIT
        buf.append("\tStandard Unit .............." + getStandard_unit() + "\n"); // ****************
        // STANDARD_UNIT
        buf.append("\tDisplay_unit ..............." + getDisplay_unit() + "\n"); // ****************
        // DISPLAY_UNIT
        buf.append("\tFormat ....................." + getFormat() + "\n"); // ****************
        // FORMAT
        buf.append("\tMin_value .................." + getMin_value() + "\n"); // ****************
        // MIN_VALUE
        buf.append("\tMax_value .................." + getMax_value() + "\n"); // ****************
        // MAX_VALUE
        buf.append("\tMin_alarm .................." + getMin_alarm() + "\n"); // ****************
        // MIN_ALARM
        buf.append("\tMax_alarm .................." + getMax_alarm() + "\n"); // ****************
        // MAX_ALARM
        buf.append("\tOptional_properties ........" + getOptional_properties() + "\n"); // ****************
        // OPTIONAL_PROPERTIES
        buf.append("\t**** Donn�es propres � l'AMT... **** \n");
        return buf.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AttributeHeavy)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final AttributeHeavy attributeHeavy = (AttributeHeavy) o;

        if (archivable != attributeHeavy.archivable) {
            return false;
        }
        if (level != attributeHeavy.level) {
            return false;
        }
        if (maxDimX != attributeHeavy.maxDimX) {
            return false;
        }
        if (maxDimY != attributeHeavy.maxDimY) {
            return false;
        }
        if (substitute != attributeHeavy.substitute) {
            return false;
        }
        if (!attributeDeviceName.equals(attributeHeavy.attributeDeviceName)) {
            return false;
        }
        if (!ctrlSys.equals(attributeHeavy.ctrlSys)) {
            return false;
        }
        if (description != null ? !description.equals(attributeHeavy.description) : attributeHeavy.description != null) {
            return false;
        }
        if (displayUnit != null ? !displayUnit.equals(attributeHeavy.displayUnit) : attributeHeavy.displayUnit != null) {
            return false;
        }
        if (!domain.equals(attributeHeavy.domain)) {
            return false;
        }
        if (!family.equals(attributeHeavy.family)) {
            return false;
        }
        if (format != null ? !format.equals(attributeHeavy.format) : attributeHeavy.format != null) {
            return false;
        }
        if (label != null ? !label.equals(attributeHeavy.label) : attributeHeavy.label != null) {
            return false;
        }
        if (maxAlarm != null ? !maxAlarm.equals(attributeHeavy.maxAlarm) : attributeHeavy.maxAlarm != null) {
            return false;
        }
        if (maxValue != null ? !maxValue.equals(attributeHeavy.maxValue) : attributeHeavy.maxValue != null) {
            return false;
        }
        if (!member.equals(attributeHeavy.member)) {
            return false;
        }
        if (minAlarm != null ? !minAlarm.equals(attributeHeavy.minAlarm) : attributeHeavy.minAlarm != null) {
            return false;
        }
        if (minValue != null ? !minValue.equals(attributeHeavy.minValue) : attributeHeavy.minValue != null) {
            return false;
        }
        if (optionalProperties != null ? !optionalProperties.equals(attributeHeavy.optionalProperties)
                : attributeHeavy.optionalProperties != null) {
            return false;
        }
        if (!registrationTime.equals(attributeHeavy.registrationTime)) {
            return false;
        }
        if (standardUnit != null ? !standardUnit.equals(attributeHeavy.standardUnit)
                : attributeHeavy.standardUnit != null) {
            return false;
        }
        if (unit != null ? !unit.equals(attributeHeavy.unit) : attributeHeavy.unit != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 29 * result + registrationTime.hashCode();
        result = 29 * result + attributeDeviceName.hashCode();
        result = 29 * result + domain.hashCode();
        result = 29 * result + family.hashCode();
        result = 29 * result + member.hashCode();
        result = 29 * result + maxDimX;
        result = 29 * result + maxDimY;
        result = 29 * result + level;
        result = 29 * result + ctrlSys.hashCode();
        result = 29 * result + archivable;
        result = 29 * result + substitute;
        result = 29 * result + (description != null ? description.hashCode() : 0);
        result = 29 * result + (label != null ? label.hashCode() : 0);
        result = 29 * result + (unit != null ? unit.hashCode() : 0);
        result = 29 * result + (standardUnit != null ? standardUnit.hashCode() : 0);
        result = 29 * result + (displayUnit != null ? displayUnit.hashCode() : 0);
        result = 29 * result + (format != null ? format.hashCode() : 0);
        result = 29 * result + (minValue != null ? minValue.hashCode() : 0);
        result = 29 * result + (maxValue != null ? maxValue.hashCode() : 0);
        result = 29 * result + (minAlarm != null ? minAlarm.hashCode() : 0);
        result = 29 * result + (maxAlarm != null ? maxAlarm.hashCode() : 0);
        result = 29 * result + (optionalProperties != null ? optionalProperties.hashCode() : 0);
        return result;
    }
}
