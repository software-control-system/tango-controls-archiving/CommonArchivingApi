package fr.soleil.archiving.common.api.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.util.FileSize;
import fr.esrf.Tango.DevFailed;

public final class LoggingUtils {

    public static final String CANNOT_OPEN_FILE = "API_CannotOpenFile";

    private final static Logger logger = LoggerFactory.getLogger(LoggingUtils.class);

    private final static Map<String, RollingFileAppender<ILoggingEvent>> fileAppenders = new HashMap<String, RollingFileAppender<ILoggingEvent>>();
    private final static Map<String, ConsoleAppender<ILoggingEvent>> consoleAppenders = new HashMap<String, ConsoleAppender<ILoggingEvent>>();
    private final static Map<String, Integer> loggingLevels = new HashMap<String, Integer>();

    private static ch.qos.logback.classic.Logger rootLoggerBack;

    public LoggingUtils() {
        final Logger rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        if (rootLogger instanceof ch.qos.logback.classic.Logger) {
            rootLoggerBack = (ch.qos.logback.classic.Logger) rootLogger;
        }
    }

    public static Logger configureLogging(final String deviceName, final boolean isUsingFile, final String filePath,
            final String level) throws IOException {
        // configure logging from device properties
        final Logger deviceLogger = LoggerFactory.getLogger(deviceName);
        if (deviceLogger != null) {
            setLoggingLevel(deviceName, LoggingLevel.getLevelFromString(level).toInt());
            // file logging
            if (isUsingFile) {
                final String fileName = filePath + "/diary_" + refactorArchiverName(deviceName) + ".log";
                try {
                    addFileAppender(fileName, deviceName);
                } catch (DevFailed e) {
                    e.printStackTrace();
                }
                // Rollover at midnight each day.
//                deviceLogger.addAppender(new DailyRollingFileAppender(
//                        new PatternLayout("%d{yyyy-MM-dd HH:mm:ss.SSS} %5p %c{1}: %m%n"), fileName, "'.'yyyy-MM-dd"));
            }
            // console logging
            addConsoleAppender(deviceName);
//            deviceLogger
//                    .addAppender(new ConsoleAppender(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss.SSS} %5p %c{1}: %m%n")));
        }
        return logger;
    }

    private static String refactorArchiverName(final String archiver) {
        return archiver.substring(archiver.lastIndexOf('/') + 1);
    }

    /**
     * Set the logging level of a device
     *
     * @param deviceName the device name
     * @param loggingLevel the level
     */
    private static void setLoggingLevel(final String deviceName, final int loggingLevel) {
//        System.out.println("set logging level " + deviceName + "-" + LoggingLevel.getLevelFromInt(loggingLevel));
        logger.debug("set logging level to {} on {}", LoggingLevel.getLevelFromInt(loggingLevel), deviceName);
        loggingLevels.put(deviceName, loggingLevel);
    }

    /**
     * Add an file appender for a device
     *
     * @param fileName
     * @param deviceName
     * @throws DevFailed
     */
    private static void addFileAppender(final String fileName, final String deviceName) throws DevFailed {
        if (rootLoggerBack != null) {
            logger.debug("add file appender of {} in {}", deviceName, fileName);
            final String deviceNameLower = deviceName.toLowerCase();
            final File f = new File(fileName);
            if (!f.exists()) {
                try {
                    f.createNewFile();
                } catch (final IOException e) {
                    throw DevFailedUtils.newDevFailed(CANNOT_OPEN_FILE, "impossible to open file " + fileName);
                }
            }
            if (!f.canWrite()) {
                throw DevFailedUtils.newDevFailed(CANNOT_OPEN_FILE, "impossible to open file " + fileName);
            }
            // debug level by default
            logger.debug("create file  " + f);
            final LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            final RollingFileAppender<ILoggingEvent> rfAppender = new RollingFileAppender<ILoggingEvent>();
            fileAppenders.put(deviceNameLower, rfAppender);
            rfAppender.setName("FILE-" + deviceNameLower);
//            rfAppender.setLevel(rootLoggingLevel);
            rfAppender.setFile(fileName);
            rfAppender.setAppend(true);
            rfAppender.setContext(loggerContext);
            final FixedWindowRollingPolicy rollingPolicy = new FixedWindowRollingPolicy();
            // rolling policies need to know their parent
            // it's one of the rare cases, where a sub-component knows about its parent
            rollingPolicy.setParent(rfAppender);
            rollingPolicy.setContext(loggerContext);
            rollingPolicy.setFileNamePattern(fileName + "%i");
            rollingPolicy.setMaxIndex(1);
            rollingPolicy.setMaxIndex(3);
            rollingPolicy.start();

            final SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy = new SizeBasedTriggeringPolicy<ILoggingEvent>();
            triggeringPolicy.setMaxFileSize(FileSize.valueOf("5 mb"));
            triggeringPolicy.setContext(loggerContext);
            triggeringPolicy.start();

            final PatternLayoutEncoder encoder = new PatternLayoutEncoder();
            encoder.setContext(loggerContext);
            encoder.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS} %5p %c{1}: %m%n");
//            encoder.setPattern("%-5level %d %X{deviceName} - %thread | %logger{25}.%M:%L - %msg%n");
            encoder.start();

            rfAppender.setEncoder(encoder);
            rfAppender.setRollingPolicy(rollingPolicy);
            rfAppender.setTriggeringPolicy(triggeringPolicy);
            rfAppender.start();

            rootLoggerBack.addAppender(rfAppender);
        }
    }

    /**
     * Add an console appender for a device
     *
     * @param deviceName
     */
    private static void addConsoleAppender(final String deviceName) {
        if (rootLoggerBack != null) {
            logger.debug("add console appender of {}", deviceName);
            ConsoleAppender<ILoggingEvent> cAppender = new ConsoleAppender<ILoggingEvent>();
            final LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            final String deviceNameLower = deviceName.toLowerCase();

            consoleAppenders.put(deviceNameLower, cAppender);
            cAppender.setContext(loggerContext);
            cAppender.setName("console");
            LayoutWrappingEncoder<ILoggingEvent> encoder = new LayoutWrappingEncoder<ILoggingEvent>();
            encoder.setContext(loggerContext);

            PatternLayout layout = new PatternLayout();

            layout.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS} %5p %c{1}: %m%n");
//        layout.setPattern("%-5level %d [%thread - %X{deviceName}] %logger{36}.%M:%L - %msg%n");

            layout.setContext(loggerContext);
            layout.start();
            encoder.setLayout(layout);
            cAppender.setEncoder(encoder);
            cAppender.start();

            rootLoggerBack.addAppender(cAppender);
        }
    }
}
