//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/AttributeLight.java,v $
//
// Project:      Tango Archiving Service
//
// Description: The AttributeLight object describes a TANGO attribute for the filing service, and containing the minimum of information characterizing it.
//				An AttributeLight thus contains :
//				- a name,
//				- a target (TANGO device in charge of inserting datum into the database)
//				- a collector (TANGO device in charge of collecting events)
//				- a mode of filling (Constraints applied to the filing).
//
// $Author: chinkumo $
//
// $Revision: 1.4 $
//
// $Log: AttributeLight.java,v $
// Revision 1.4  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.3.12.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.3  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.2.4.1  2005/04/29 18:35:03  chinkumo
// Changes made to remove deprecated objetcs (ListeAttributs, ListeDevices, Device)
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.common.api.tools;

/**
 * <p/>
 * <B>Description :</B><BR>
 * The <I>AttributeLight</I> object describes a TANGO attribute for the filing
 * service, and containing the minimum of information characterizing it. An
 * <I>AttributeLight</I> thus contains :
 * <ul>
 * <li>a <I>name</I>,
 * <li>a <I>target</I> (TANGO device in charge of inserting datum into the
 * database)
 * <li>a <I>collector</I> (TANGO device in charge of collecting events)
 * <li>a <I>mode of filling</I> (Constraints applied to the filing).
 * </ul>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.4 $
 * @see fr.soleil.archiving.common.api.tools.ArchivingEvent
 */
public class AttributeLight {
	private String attributeCompleteName = "";
	private int attributeId = 0;
	private int dataType = 0;
	private int dataFormat = 0;
	private int writable = 0;

	/**
	 * Default constructor Creates a new instance of AttributeLight
	 * 
	 * @see #AttributeLight(String)
	 * @see #AttributeLight(String[])
	 */
	public AttributeLight() {
	}

	/**
	 * This constructor takes one parameter as inputs.
	 * 
	 * @param attribute_complete_name the attribute's complete name
	 * @see #AttributeLight()
	 * @see #AttributeLight(String[])
	 */
	public AttributeLight(String attribute_complete_name) {
		this.attributeCompleteName = attribute_complete_name;
	}

	/**
	 * This constructor builds an AttributeLight from an array
	 * 
	 * @param argin an array that contains the AttributeLight's full name, data type
	 *              parameter, data format parameter, writable parameter.
	 */
	public AttributeLight(String[] argin) {
		setAttributeCompleteName(argin[0]); // **************** The whole
		// attribute name (device_name +
		// attribute_name)
		setAttributeId(Integer.parseInt(argin[1])); // ****************
		// Attribute identifier
		// used for the database
		// SNAP
		setDataType(Integer.parseInt(argin[2])); // **************** Attribute
		// data type
		setDataFormat(Integer.parseInt(argin[3])); // ****************
		// Attribute data format
		setWritable(Integer.parseInt(argin[4])); // **************** Attribute
		// read/write type
	}

	/**
	 * Returns the AttributeLight's name.
	 * 
	 * @return the AttributeLight's name.
	 * @see #setAttribute_complete_name
	 */
	public String getAttributeCompleteName() {
		return attributeCompleteName;
	}

	/**
	 * Sets the AttributeLight's name.
	 * 
	 * @param attribute_complete_name the AttributeLight's name.
	 * @see #getAttribute_complete_name
	 */
	public void setAttributeCompleteName(String attribute_complete_name) {
		this.attributeCompleteName = attribute_complete_name;
	}

	/**
	 * Returns the AttributeLight's identifier used for the database SNAP.
	 * 
	 * @return the AttributeLight's identifier used for the database SNAP.
	 */
	public int getAttributeId() {
		return attributeId;
	}

	/**
	 * Sets the AttributeLight's identifier used for the database SNAP.
	 * 
	 * @param attribute_id identifier used for the database SNAP.
	 */
	public void setAttributeId(int attribute_id) {
		this.attributeId = attribute_id;
	}

	/**
	 * Returns the AttributeLight's <I>type property</I>
	 * (DevShort/DevLong/DevDouble)
	 * 
	 * @return the AttributeLight's <I>type property</I>
	 *         (DevShort/DevLong/DevDouble)
	 */
	public int getDataType() {
		return dataType;
	}

	/**
	 * Sets the AttributeLight's <I>type property</I> (DevShort/DevLong/DevDouble)
	 * 
	 * @param data_type the AttributeLight's <I>type property</I>
	 *                  (DevShort/DevLong/DevDouble)
	 */
	public void setDataType(int data_type) {
		this.dataType = data_type;
	}

	/**
	 * Returns the AttributeLight's <I>data format property</I>
	 * (SCALAR/SPECTRUM/IMAGE)
	 * 
	 * @return the AttributeLight's <I>data format property</I>
	 *         (SCALAR/SPECTRUM/IMAGE)
	 */
	public int getDataFormat() {
		return dataFormat;
	}

	/**
	 * Sets the AttributeLight's <I>data format property</I> (SCALAR/SPECTRUM/IMAGE)
	 * 
	 * @param data_format the AttributeLight's <I>data format property</I>
	 *                    (SCALAR/SPECTRUM/IMAGE)
	 */
	public void setDataFormat(int data_format) {
		this.dataFormat = data_format;
	}

	/**
	 * Returns the AttributeLight's <I>writable property</I>
	 * (READ/READ_WRITE/READ_WITH_WRITE).
	 * 
	 * @return the AttributeLight's <I>writable property</I>
	 *         (READ/READ_WRITE/READ_WITH_WRITE).
	 */
	public int getWritable() {
		return writable;
	}

	/**
	 * Sets the AttributeLight's <I>writable property</I>
	 * (READ/READ_WRITE/READ_WITH_WRITE).
	 * 
	 * @param writable the AttributeLight's <I>writable property</I>
	 *                 (READ/READ_WRITE/READ_WITH_WRITE).
	 */
	public void setWritable(int writable) {
		this.writable = writable;
	}

	/**
	 * Returns an array representation of the object <I>AttributeLight</I>. In this
	 * order, array fields are :
	 * <ol>
	 * <li>the <I>attribute complete name</I> (device_name + attribute_name),
	 * <li>the <I>attribute type property</I> (DevShort/DevLong/DevDouble),
	 * <li>the <I>attribute data format property</I> (SCALAR/SPECTRUM/IMAGE),
	 * <li>the <I>attribute writable property</I> (READ/READ_WRITE/READ_WITH_WRITE),
	 * </ol>
	 * 
	 * @return an array representation of the object <I>AttributeLight</I>.
	 */
	public String[] toArray() {
		String[] snapLightAtt;
		snapLightAtt = new String[5];

		snapLightAtt[0] = attributeCompleteName;
		snapLightAtt[1] = Integer.toString(attributeId);
		snapLightAtt[2] = Integer.toString(dataType);
		snapLightAtt[3] = Integer.toString(dataFormat);
		snapLightAtt[4] = Integer.toString(writable);

		return snapLightAtt;
	}

	public String toString() {
		String snapString = new String("");
		snapString = "Attribut : " + getAttributeCompleteName() + "\r\n" + "\t" + "Attribute Id : \t" + getAttributeId()
				+ "\r\n" + "\t" + "data_type : \t" + getDataType() + "\r\n" + "\t" + "data_format : \t"
				+ getDataFormat() + "\r\n" + "\t" + "writable : \t" + getWritable() + "\r\n";
		return snapString;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AttributeLight)) {
			return false;
		}

		final AttributeLight attributeLight = (AttributeLight) o;

		if (attributeId != attributeLight.attributeId) {
			return false;
		}
		if (dataFormat != attributeLight.dataFormat) {
			return false;
		}
		if (dataType != attributeLight.dataType) {
			return false;
		}
		if (writable != attributeLight.writable) {
			return false;
		}
		if (!attributeCompleteName.equals(attributeLight.attributeCompleteName)) {
			return false;
		}

		return true;
	}
}