// +============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ArchivingEvent.java,v $
//
// Project:      Tango Archiving Service
//
// Description: The ArchivingEvent object describes an event for the filing service.
//				An ArchivingEvent thus contains :
//				- a name,
//				- a timestamp
//				- a value
//				- a property format  (scalar, spectrum, image)
//
// $Author: chinkumo $
//
// $Revision: 1.4 $
//
// $Log: ArchivingEvent.java,v $
// Revision 1.4  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.3.12.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.3.12.1  2005/09/09 08:34:24  chinkumo
// To improve the collecting politic (see Collectors in H/TdbArchivers) this object was enhanced.
//
// Revision 1.3  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.2.4.1  2005/04/29 18:35:03  chinkumo
// Changes made to remove deprecated objetcs (ListeAttributs, ListeDevices, Device)
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.common.api.tools;

import fr.soleil.lib.project.Format;

/**
 * <p/>
 * <B>Description :</B><BR>
 * The {@link ArchivingEvent} object describes an event for the filing service. An {@link ArchivingEvent} thus contains
 * :
 * <ul>
 * <li>a <I>attributeCompleteName</I>,
 * <li>a <I>timestamp</I>
 * <li>a <I>value</I>
 * <li>a property <I>format</I> (scalar, spectrum, image)
 * </ul>
 * </p>
 *
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.4 $
 */
public abstract class ArchivingEvent<T> {
    // <li> a property <I>type</I> (DevShort, DevLong, DevDouble, DevString)
    // <li> a property <I>writable</I> (read, read_with_write, read_write)

    private String attributeCompleteName;
    private int dataType;
    private int dataFormat;
    private int writable;
    private long timestamp;
    private String tableName;
    private Object value;
    private T nullElements;

    /**
     * Default constructor Creates a new instance of ArchivingEvent
     */
    public ArchivingEvent() {
        attributeCompleteName = "";
        tableName = "";
        dataType = 0;
        dataFormat = 0;
        writable = 0;
        timestamp = 0;
        value = null;
        nullElements = null;
    }

    /**
     * Returns the ArchivingEvent's attributeCompleteName.
     *
     * @return the ArchivingEvent's attributeCompleteName.
     * @see #setAttributeCompleteName
     * @see #getTimeStamp
     * @see #getValue
     */
    public String getAttributeCompleteName() {
        return attributeCompleteName;
    }

    /**
     * Sets the ArchivingEvent's attributeCompleteName.
     *
     * @param attributeCompleteName
     *            the ArchivingEvent's attributeCompleteName.
     * @see #getAttributeCompleteName
     * @see #setTimeStamp
     * @see #setValue
     */
    public void setAttributeCompleteName(final String attributeCompleteName) {
        this.attributeCompleteName = attributeCompleteName;
    }

    /**
     * @return
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * @param data_type
     */
    public void setDataType(final int dataType) {
        this.dataType = dataType;
    }

    /**
     * @return
     */
    public int getDataFormat() {
        return dataFormat;
    }

    /**
     * @param data_format
     */
    public void setDataFormat(final int dataFormat) {
        this.dataFormat = dataFormat;
    }

    /**
     * @return
     */
    public int getWritable() {
        return writable;
    }

    /**
     * @param writable
     */
    public void setWritable(final int writable) {
        this.writable = writable;
    }

    /**
     * Returns the ArchivingEvent's timestamp.
     *
     * @return the ArchivingEvent's timestamp.
     * @see #getAttributeCompleteName
     * @see #setTimeStamp
     * @see #getValue
     */
    public long getTimeStamp() {
        return timestamp;
    }

    /**
     * Sets the ArchivingEvent's timestamp.
     *
     * @param timestamp
     *            the ArchivingEvent's timestamp.
     * @see #setAttributeCompleteName
     * @see #getTimeStamp
     * @see #setValue
     */
    public void setTimeStamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * @param tableName
     */
    public void setTableName(final String tableName) {
        this.tableName = tableName;
    }

    /**
     * Returns the ArchivingEvent's value. Let us note here that what returned
     * is of type "Object". A cast operation is thus necessary.
     *
     * @see #getAttributeCompleteName
     * @see #getTimeStamp()
     * @see #setValue(Object, Object)
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the ArchivingEvent's value. Let us note here that the given value
     * has to be of type "Object". A cast operation can thus be necessary.
     *
     * @param value
     *            ArchivingEvent's value
     * @param nullElements
     *            The nullElements (supposed to be a boolean n-dim array of same
     *            dimension as value, with <code>true</code> for <code>null</code>, <code>false</code> otherwise). This
     *            is
     *            useful for type array values.
     * @see #setAttributeCompleteName(String)
     * @see #setTimeStamp(long)
     */
    public void setValue(final Object value, final T nullElements) {
        this.value = value;
        this.nullElements = nullElements;
    }

    /**
     * Returns the null elements
     *
     * @return The nullElements (supposed to be a boolean n-dim array of same
     *         dimension as value, with <code>true</code> for <code>null</code>, <code>false</code> otherwise). This is
     *         useful for type array
     *         values.
     * @see #setValue(Object, Object)
     */
    public T getNullElements() {
        return nullElements;
    }

    /**
     * Returns an array representation of this {@link ArchivingEvent}.
     *
     * @return an array representation of this {@link ArchivingEvent}.
     */
    public abstract String[] toArray();

    /**
     * Returns a string representation of this {@link ArchivingEvent}.
     *
     * @return a string representation of this {@link ArchivingEvent}.
     */
    @Override
    public abstract String toString();

    protected String toString(final byte value) {
        return toString((double) value);
    }

    protected String toString(final short value) {
        return toString((double) value);
    }

    protected String toString(final int value) {
        return toString((double) value);
    }

    protected String toString(final long value) {
        return toString((double) value);
    }

    protected String toString(final float value) {
        return Format.formatValue(value, GlobalConst.FLOAT_FORMAT).trim();
    }

    protected String toString(final double value) {
        return Format.formatValue(value, GlobalConst.FLOAT_FORMAT).trim();
    }

    protected String toString(final Object value) {
        String result;
        if (value instanceof Number) {
            result = toString(((Number) value).doubleValue());
        } else {
            result = String.valueOf(value);
        }
        return result;
    }
}
