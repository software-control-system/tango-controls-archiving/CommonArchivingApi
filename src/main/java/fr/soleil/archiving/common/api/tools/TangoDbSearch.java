// +======================================================================
//$Source:  $
//
//Project :     CommonArchivingApi
//
//Description: This class provides common methodes to access to the Tango DB
//
//$Author:  $
//
//$Revision: $
//
//$Log: $
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.archiving.common.api.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;

/**
 * This class provides common methods to access to the Tango DB
 *
 * @author PIERREJOSEPH
 *
 */
public class TangoDbSearch {

    Logger logger = LoggerFactory.getLogger(TangoDbSearch.class);

    public String[] getAttributesForClass(final String classe, final Collection<?> devOfDomain) throws DevFailed {
        String[] exported_dev;
        String[] attributeList = new String[5];
        final Database database = ApiUtil.get_db_obj();
        DeviceProxy deviceProxy;
        exported_dev = database.get_device_exported_for_class(classe);
        for (int i = 0; i < exported_dev.length; i++) {
            final String current_dev = exported_dev[i];
            if (devOfDomain.contains(current_dev)) {
                try {
                    deviceProxy = new DeviceProxy(current_dev);
                    attributeList = deviceProxy.get_attribute_list();
                    break;
                } catch (final DevFailed devFailed) {
                }
            }
        }
        return attributeList;

    }

    public Collection<String> getDomains() throws DevFailed {
        final Database database = ApiUtil.get_db_obj();
        final Collection<String> domains = new ArrayList<>();
        final String[] resDomains = database.get_device_domain("*");
        for (int i = 0; i < resDomains.length; i++) {
            final String domain = resDomains[i];
            if (!domain.equals("dserver")) {
                domains.add(domain);
            }
        }
        return domains;

    }

    public Collection<String> getDomains(final String domainsRegExp) throws DevFailed {
        final Database database = ApiUtil.get_db_obj();
        final Collection<String> domains = new ArrayList<>();
        final String[] resDomains = database.get_device_domain(domainsRegExp);
        for (int i = 0; i < resDomains.length; i++) {
            final String domain = resDomains[i];
            if (!domain.equals("dserver")) {
                domains.add(domain);
            }
        }
        return domains;

    }

    /**
     * Retrieves devices sorted by classes from Tango DB
     *
     * @param domain
     * @return A collection with class as key and a list of devices as value
     * @throws DevFailed
     */
    public Map<String, Collection<String>> getClassAndDevices(final String domain) throws DevFailed {
        final long timeIn = System.currentTimeMillis();
        final Database database = ApiUtil.get_db_obj();
        final Map<String, Collection<String>> deviceClassToDevices = new HashMap<>();
        logger.debug("Getting devices and classes for domain {}", domain);
        final String[] families = getFamilies(domain);
        for (int i = 0; i < families.length; i++) {
            final String[] members = getMembers(domain, families[i]);
            for (int j = 0; j < members.length; j++) {
                final String deviceName = domain + "/" + families[i] + "/" + members[j];
                // XXX database.get_device_info does'nt work as expected anymore,
                // but database.get_class_for_device does work (TANGOARCH-650)
//                final String deviceClass = database.get_device_info(deviceName).classname;
                final String deviceClass = database.get_class_for_device(deviceName);
                Collection<String> devicesForTheCurrentDeviceClass = deviceClassToDevices.get(deviceClass);
                if (devicesForTheCurrentDeviceClass == null) {
                    devicesForTheCurrentDeviceClass = new ArrayList<>();
                    deviceClassToDevices.put(deviceClass, devicesForTheCurrentDeviceClass);
                }
                devicesForTheCurrentDeviceClass.add(deviceName);
            }
        }
        final long timeOut = System.currentTimeMillis();
        logger.debug("getting devices and classes for domain {} took {} ms", domain, timeOut - timeIn);
        return deviceClassToDevices;
    }

    private String[] getFamilies(final String domain) throws DevFailed {
        final Database database = ApiUtil.get_db_obj();
        return database.get_device_family(domain + "/*");
    }

    private String[] getMembers(final String domain, final String family) throws DevFailed {
        final Database database = ApiUtil.get_db_obj();
        return database.get_device_member(domain + "/" + family + "/*");
    }
}
