package fr.soleil.archiving.common.api.tools;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.esrf.Tango.AttrQuality;
import fr.esrf.Tango.DevError;

public class NullableTimedData implements Serializable, Cloneable {

    private static final long serialVersionUID = -5574074624691499212L;

    private long time;
    private Object value = null;
    private Object nullElements = null;
    private AttrQuality qual = AttrQuality.ATTR_VALID;
    private int x = 0;
    private int y = 0;
    private int dataType = -1;
    private DevError[] err = null;

    public long getTime() {
        return time;
    }

    public void setTime(final long time) {
        this.time = time;
    }

    /**
     * Returns the null elements of this {@link NullableTimedData}
     *
     * @return The null elements. generally, a <code>boolean[]</code>, with <code>true</code> for null,
     *         <code>false</code> otherwise
     * @see #setValue(Object, Object)
     */
    public Object getNullElements() {
        return nullElements;
    }

    public Object getValue() {
        return value;
    }

    /**
     * Sets the value and the null elements of this {@link NullableTimedData}
     *
     * @param value
     *            The value to set
     * @param nullElements
     *            The null elements to set. <code>nullElements</code> is
     *            expected to be a <code>boolean[]</code> if <code>value</code> is an array, a <code>boolean[][]</code>
     *            if <code>value</code> is a matrix, and so on. Can be <code>null</code>. <code>nullElements</code> is
     *            expected to encode <code>true</code> for <code>null</code> , <code>false</code> otherwise
     */
    public void setValue(final Object value, final Object nullElements) {
        this.value = value;
        this.nullElements = nullElements;
    }

    public AttrQuality getQual() {
        return qual;
    }

    public void setQual(final AttrQuality qual) {
        this.qual = qual;
    }

    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(final int y) {
        this.y = y;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(final int data_type) {
        this.dataType = data_type;
    }

    public DevError[] getErr() {
        return err;
    }

    public void setErr(final DevError[] err) {
        this.err = err;
    }

    public void clean() {
        setValue(null, null);
        DevError[] error = err;
        if (error != null) {
            Arrays.fill(error, null);
            error = null;
            setErr(error);
        }
        qual = null;
    }

    protected final Object cloneArray(final Object array) {
        Object result = array;
        if (array != null) {
            if (array instanceof Object[]) {
                final Object[] temp = ((Object[]) array).clone();
                for (int i = 0; i < temp.length; i++) {
                    temp[i] = cloneArray(temp[i]);
                }
                result = temp;
            } else if (array instanceof char[]) {
                result = ((char[]) array).clone();
            } else if (array instanceof boolean[]) {
                result = ((boolean[]) array).clone();
            } else if (array instanceof byte[]) {
                result = ((byte[]) array).clone();
            } else if (array instanceof short[]) {
                result = ((short[]) array).clone();
            } else if (array instanceof int[]) {
                result = ((int[]) array).clone();
            } else if (array instanceof long[]) {
                result = ((long[]) array).clone();
            } else if (array instanceof float[]) {
                result = ((float[]) array).clone();
            } else if (array instanceof double[]) {
                result = ((double[]) array).clone();
            }
        }
        return result;
    }

    @Override
    public String toString() {
        final ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("time", time);
        builder.append("value", ArrayUtils.toString(value));
        builder.append("data_type", dataType);
        builder.append("x", x);
        builder.append("y", y);
        return builder.toString();
    }

    @Override
    public NullableTimedData clone() {
        NullableTimedData result;
        try {
            result = (NullableTimedData) super.clone();
        } catch (final CloneNotSupportedException e) {
            // Should not happen as NullableTimedData is cloneable
            result = new NullableTimedData();
            result.qual = qual;
            result.x = x;
            result.y = y;
            result.dataType = dataType;
        }
        result.value = cloneArray(value);
        result.nullElements = cloneArray(nullElements);
        if (err != null) {
            result.err = err.clone();
        }
        return result;
    }

    @Override
    protected void finalize() {
        clean();
    }

}
