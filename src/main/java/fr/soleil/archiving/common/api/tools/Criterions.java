//+======================================================================
// $Source:  $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class Criterions.
//						(Garda Laure) - 1 juil. 2005
//
// $Author:  $
//
// $Revision:  $
//
// $Log:  $
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.common.api.tools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Description : A Criterion object describes a set of search criteria for a request into the database A Criterion
 * contains : a set of Condition objects.
 * 
 * @author GARDA
 */
public class Criterions implements Serializable {

    private static final long serialVersionUID = 8122413993936306521L;

    /**
     * Set of condition
     */
    private final Map<String, Collection<Condition>> conditionsHT = new ConcurrentHashMap<>();

    /**
     * Default constructor.
     */
    public Criterions() {
    }

    /**
     * This constructor takes one parameter as inputs.
     * 
     * @param conditions
     *            a set of Conditions
     */
    public Criterions(final Condition[] conditions) {
        // Sets the Conditions into the Criterion.
        if (conditions != null) {
            final int nbOfConditions = conditions.length;
            for (int i = 0; i < nbOfConditions; i++) {
                final String columnName = conditions[i].getColumn();
                // The Conditions are referenced by their table's field (columnName).
                Collection<Condition> currentColumnConditionsList = conditionsHT.get(columnName);
                if (currentColumnConditionsList == null) {
                    currentColumnConditionsList = new ArrayList<Condition>();
                    conditionsHT.put(columnName, currentColumnConditionsList);
                }
                currentColumnConditionsList.add(conditions[i]);
            }
        }
    }

    /**
     * Adds a Condition into the Criterion.
     * 
     * @param condition A Condition
     */
    public void addCondition(final Condition condition) {
        if (condition != null) {
            final String columnName = condition.getColumn();
            Collection<Condition> currentColumnConditionsList = conditionsHT.get(columnName);
            if (currentColumnConditionsList == null) {
                currentColumnConditionsList = new ArrayList<Condition>();
                conditionsHT.put(columnName, currentColumnConditionsList);
            }
            currentColumnConditionsList.add(condition);
        }
    }

    /**
     * Returns array of all Condition objects (of this Criterion) for the given table's field
     * 
     * @param columnName
     * @return array of all Condition objects for the given table's field
     */
    public Condition[] getConditions(final String columnName) {
        Condition[] ret = null;
        if ((columnName != null) && (conditionsHT != null)) {
            final Collection<Condition> columnConditionsList = conditionsHT.get(columnName);
            if (columnConditionsList != null) {
                ret = columnConditionsList.toArray(new Condition[columnConditionsList.size()]);
            }
        }
        return ret;
    }

    /**
     * Returns a String which represents the object Criterion.
     * 
     * @return String which represents the object Criterion
     */
    @Override
    public String toString() {
        return conditionsHT.toString();

    }

    public Map<String, Collection<Condition>> getConditionsHT() {
        return conditionsHT;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Criterions criterions = (Criterions) obj;
            equals = ObjectUtils.sameMapContent(conditionsHT, criterions.conditionsHT);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xC1217;
        int mult = 0xE12102;
        code = code * mult + getClass().hashCode();
        code = code * mult + conditionsHT.hashCode();
        return code;
    }

}
