package fr.soleil.archiving.common.api.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import fr.soleil.archiving.common.api.exception.ArchivingException;

// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/DateUtil.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class DateUtil.
// (chinkumo) - 3 juin 2005
//
// $Author: ounsy $
//
// $Revision: 1.9 $
//
// $Log: DateUtil.java,v $
// Revision 1.9 2007/02/16 09:50:50 ounsy
// added Oracle-->MySQL date format conversion
//
// Revision 1.8 2006/10/31 16:49:35 ounsy
// minor changes
//
// Revision 1.7 2006/09/05 12:27:59 ounsy
// updated for sampling compatibility
//
// Revision 1.6 2005/11/29 17:11:17 chinkumo
// no message
//
// Revision 1.5.2.2 2005/11/15 13:34:38 chinkumo
// no message
//
// Revision 1.5.2.1 2005/09/09 08:37:44 chinkumo
// Management of the exception that occurs when a string date is misformatted.
//
// Revision 1.5 2005/08/19 14:04:01 chinkumo
// no message
//
// Revision 1.4.6.1 2005/08/01 13:43:16 chinkumo
// The date mask was changed to allow the support of the milliseconds.
//
// Revision 1.4 2005/06/24 12:06:11 chinkumo
// Some constants were moved from
// fr.soleil.TangoArchiving.ArchivingApi.ConfigConst to
// fr.soleil.hdbtdbArchivingApi.ArchivingTools.Tools.GlobalConst.
// This change was reported here.
//
// Revision 1.3 2005/06/14 14:05:53 chinkumo
// no message
//
// Revision 1.1 2005/06/13 09:39:39 chinkumo
// This class manages all the date and time stuff (formats, format
// transformations, etc...)
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================

public final class DateUtil {

    // Globals
    public static final String FR_DATE_PATTERN = "dd-MM-yyyy HH:mm:ss.SSS";
    public static final String US_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * Cast the given long (number of milliseconds since January 1, 1970) into a
     * string that follows the given pattern (either the french one (dd-MM-yyyy
     * HH:mm:ss) or the US one (yyyy-MM-dd HH:mm:ss))
     * 
     * @param milli
     *            the date in millisecond to cast
     * @param pattern
     *            the target pattern
     * @return a string representing the date into the given pattern format
     */
    public static synchronized String milliToString(final long milli, final String pattern) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(milli), TimeZone.getDefault().toZoneId())
				.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * Cast a string format date (dd-MM-yyyy HH:mm:ss or yyyy-MM-dd HH:mm:ss)
     * into long (number of milliseconds since January 1, 1970)
     * 
     * @param date
     *            the date to parse
     * @return a <code>long</code>
     * @throws ArchivingException
     *             If a problem occurred while trying to parse date
     */
    public static synchronized long stringToMilli(final String date) throws ArchivingException {
		if(date ==null) {
			return -1;
		}
		return parseDate(date).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}

	private static LocalDateTime parseDate(String date) {
		LocalDateTime localDateTime = null;
		if (date != null) {
			String cleanDate = date.trim();
			cleanDate = cleanDate.substring(0, Math.min(FR_DATE_PATTERN.length(), cleanDate.length()));
			String pattern = (cleanDate.indexOf("-") != 4 ? FR_DATE_PATTERN : US_DATE_PATTERN).substring(0,
					Math.min(FR_DATE_PATTERN.length(), cleanDate.length()));

			localDateTime = cleanDate.length() > 10
					? LocalDateTime.parse(cleanDate, DateTimeFormatter.ofPattern(pattern))
					: LocalDate.parse(cleanDate, DateTimeFormatter.ofPattern(pattern)).atStartOfDay();
		}
		return localDateTime;
    }

    /**
     * Return a date string in the french date format (dd-MM-yyyy HH:mm:ss)
     * 
     * @param date
     *            the date string
     * @return the date string in the french date format (dd-MM-yyyy HH:mm:ss)
     * @throws ArchivingException
     */
    public static synchronized String stringToDisplayString(final String date) throws ArchivingException {
		return parseDate(date).format(DateTimeFormatter.ofPattern(FR_DATE_PATTERN));
    }


}
