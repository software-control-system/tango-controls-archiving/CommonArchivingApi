package fr.soleil.archiving.common.api.utils;

/**
 * A class used to have a linked list on <code>double</code> elements. This
 * class is not thread safe at all.
 * 
 * @author girardot
 * 
 */
public class DoubleLinkedList {
    private int size;
    private DoubleElement first;
    private DoubleElement last;

    /**
     * Constructs a new empty {@link DoubleLinkedList}
     */
    public DoubleLinkedList() {
        size = 0;
        first = null;
        last = null;
    }

    private void add(DoubleElement element, boolean updateSizeAndLast) {
        if (element != null) {
            if (last != null) {
                last.next = element;
            }
            last = element;
            if (first == null) {
                first = element;
            }
            if (updateSizeAndLast) {
                size++;
                while (last.next != null) {
                    last = last.next;
                    size++;
                }
            }
        }
    }

    /**
     * Adds a <code>double</code> value in this list. Does not check for
     * duplicates
     * 
     * @param value
     *            The value to add
     */
    public void add(double value) {
        add(new DoubleElement(value), true);
    }

    /**
     * Adds all the elements of a {@link DoubleLinkedList} in this list.
     * 
     * Warning, you will break this list content if you call {@link #clear()} on
     * the list in parameter.
     * 
     * @param list
     *            The {@link DoubleLinkedList} from which to add elements.
     */
    public void addAll(DoubleLinkedList list) {
        if (list != null) {
            add(list.first, false);
            if (list.last != null) {
                last = list.last;
                size += list.size;
            }
        }
    }

    /**
     * Returns this {@link DoubleLinkedList} content as a <code>double</code>
     * array
     * 
     * @return
     */
    public double[] toArray() {
        double[] result = new double[size];
        DoubleElement element = first;
        int index = 0;
        while (element != null) {
            result[index++] = element.getValue();
            element = element.next;
        }
        return result;
    }

    /**
     * Clears this {@link DoubleLinkedList}
     */
    public void clear() {
        size = 0;
        last = null;
        DoubleElement element = first;
        first = null;
        while (element != null) {
            DoubleElement next = element.next;
            element.next = null;
            element = next;
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class DoubleElement {
        private double value;
        private DoubleElement next;

        public DoubleElement(double value) {
            this.value = value;
            next = null;
        }

        public double getValue() {
            return value;
        }
    }
}
