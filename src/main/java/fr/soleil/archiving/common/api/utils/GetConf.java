package fr.soleil.archiving.common.api.utils;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;

public class GetConf {

    private static final String DAY_REGEX = "(jour|day)s?";
    private static final String HOUR_REGEX = "(hour|heure)s?";
    private static final String MINUTE_REGEX = "minutes?";
    private static final String SECOND_REGEX = "seconde?s?";
    private static final String MILLISECOND_REGEX = "milliseconds?";

    public static final String HOST_PROPERTY = "DbHost";
    public static final String NAME_PROPERTY = "DbName";
    public static final String SCHEMA_PROPERTY = "DbSchema";
    public static final String USER_PROPERTY = "DbUser";
    public static final String PASSWORD_PROPERTY = "DbPassword";
    public static final String ONS_CONF_PROPERTY = "DbONSConf";
    public static final String TNS_NAMES_PROPERTY = "DbTnsNames";
    public static final String IS_RAC_PROPERTY = "isRac";
    public static final String IS_RAC_PROPERTY_BACKUP = "RacConnection";
    public static final String DATABASE_TYPE_PROPERTY = "DbType";
    public static final String DATABASE_TYPE_ORACLE = "Oracle";
    public static final String DATABASE_TYPE_MYSQL = "MySQL";
    public static final String AUTO_RECONNECT_PROPERTY = "AutoReconnectFrequency";
    public static final String INACTIVITY_TIMEOUT_SIZE_PROPERTY = "DbCnxInactivityTimeout";
    public static final String MAX_POOL_SIZE_PROPERTY = "DbMaxPoolSize";
    public static final String MIN_POOL_SIZE_PROPERTY = "DbMinPoolSize";

    /**
     * The number of millisecond in one second
     */
    public static final int SECOND = 1000;

    /**
     * The number of millisecond in one minute
     */
    public static final int MINUTE = 60 * SECOND;

    /**
     * The number of millisecond in one hour
     */
    public static final int HOUR = 60 * MINUTE;

    /**
     * The number of millisecond in one day
     */
    public static final int DAY = 24 * HOUR;

    private static final int DEFAULT_AUTO_RECONNECT = 15 * MINUTE;

    /**
     * Utility method to parse a duration.
     *
     * @param initialDuration An <code>int</code> that contains a initial duration
     * @param token A String that correspond to:
     *            <ul>
     *            <li>millisecond (if allowed)</li>
     *            <li>second</li>
     *            <li>minute</li>
     *            <li>hour</li>
     *            <li>day</li>
     *            </ul>
     * @param allowMilliseconds whether milliseconds are allowed in <code>token</code>
     *
     * @return A <code>int</code> corresponding to the initialDuration multiplied by the value of token
     */
    public static int parseDuration(final int initialDuration, final String token, final boolean allowMilliseconds) {
        int duration = initialDuration;
        if (token.matches(SECOND_REGEX)) {
            duration *= SECOND;
        } else if (token.matches(MINUTE_REGEX)) {
            duration *= MINUTE;
        } else if (token.matches(HOUR_REGEX)) {
            duration *= HOUR;
        } else if (token.matches(DAY_REGEX)) {
            duration *= DAY;
        } else if (!allowMilliseconds || !token.matches(MILLISECOND_REGEX)) {
            throw new IllegalArgumentException();
        }
        return duration;
    }

    /**
     * Utility to parse duration form a {@link String}
     *
     * @param property The {@link String} from which to parse duration
     * @param defaultValue The default value to return, in case of problem.
     * @param allowMilliseconds Whether to allow milliseconds in specified time units
     * @return An <code>int</code>.
     */
    public static final int parseDuration(final String property, final int defaultValue, final boolean allowMilliseconds) {
        int duration;
        try {
            final String[] split = property.split(" ");
            if (split.length == 2) {
                duration = parseDuration(Integer.parseInt(split[0].trim()), split[1].trim().toLowerCase(),
                        allowMilliseconds);
            } else if (split.length == 1) {
                // duration is already
                duration = Integer.parseInt(split[0].trim());
            } else {
                duration = defaultValue;
            }
        } catch (final Exception e) {
            duration = defaultValue;
        }
        return duration;
    }

    /**
     * Utility to parse duration form a {@link String}
     *
     * @param property The {@link String} from which to parse duration
     * @param defaultValue The default value to return, in case of problem.
     * @return An <code>int</code>.
     */
    public static final int parseDuration(final String property, final int defaultValue) {
        return parseDuration(property, defaultValue, true);
    }

    /**
     * Utility to parse a duration from a property
     *
     * @param className The name of the class
     * @param propName The name of the property
     * @return An <code>int</code>.
     * @throws DevFailed If a problem occurred during Tango reading or if the property did not represent a duration.
     */
    public static int readDurationInDB(final String className, final String propName) throws DevFailed {
        final int duration = parseDuration(readStringInDB(className, propName), -1);
        if (duration < 0) {
            final DevError error = new DevError("Invalid duration value", ErrSeverity.ERR,
                    "Failed to read a valid duration (must be >= 0)", "GetConf.readDurationInDB");
            throw new DevFailed("Duration reading error", new DevError[] { error });
        } else {
            return duration;
        }
    }

    /**
     * Utility to parse a duration from a property
     *
     * @param className The name of the class
     * @param propName The name of the property
     * @param defaultValue default value to use when property does not exist
     * @return An <code>int</code>.
     * @throws DevFailed If a problem occurred during Tango reading or if the property did not represent a duration.
     */
    public static int readDurationInDB(final String className, final String propName, final int defaultValue)
            throws DevFailed {
        final int duration = parseDuration(readStringInDB(className, propName), defaultValue);
        if (duration < 0) {
            final DevError error = new DevError("Invalid " + propName + " value", ErrSeverity.ERR,
                    "Failed to read a valid duration (must be >= 0)", "GetConf.readDurationInDB");
            throw new DevFailed(propName + " reading error", new DevError[] { error });
        } else {
            return duration;
        }
    }

    /**
     * Utility to parse auto reconnect frequency
     *
     * @param className The name of the class
     * @return An <code>int</code>.
     * @throws DevFailed If a problem occurred during Tango reading
     */
    public static int getAutoReconnectFrequency(final String className) throws DevFailed {
        return readDurationInDB(className, AUTO_RECONNECT_PROPERTY, DEFAULT_AUTO_RECONNECT);
    }

    /**
     * return the host property define for the given class
     *
     * @param className
     *            , the name of the class
     */
    public static String getHost(final String className) throws DevFailed {
        return readStringInDB(className, HOST_PROPERTY);
    }

    /**
     * return the name property define for the given class
     *
     * @param className
     *            , the name of the class
     */
    public static String getName(final String className) throws DevFailed {
        return readStringInDB(className, NAME_PROPERTY);
    }

    /**
     * return the name property define for the given class
     *
     * @param className
     *            , the name of the class
     */
    public static String getSchema(final String className) throws DevFailed {
        return readStringInDB(className, SCHEMA_PROPERTY);
    }

    public static String getUser(final String className) throws DevFailed {
        return readStringInDB(className, USER_PROPERTY);
    }

    public static String getPwd(final String className) throws DevFailed {
        return readStringInDB(className, PASSWORD_PROPERTY);
    }

    /**
     * return the name property define for the given class
     *
     * @param className The name of the class
     */
    public static String isRAC(final String className) throws DevFailed {
        String result = readStringInDB(className, IS_RAC_PROPERTY);
        if (result == null || result.isEmpty()) {
            result = readStringInDB(className, IS_RAC_PROPERTY_BACKUP);
        }
        return result;
    }

    /**
     * Returns whether a property value indicates an Oracle database
     *
     * @param propertyValue The property value
     * @return A {@link Boolean}. <code>null</code> if the property value is <code>null</code> or empty.
     */
    public static Boolean isOracleProperty(String propertyValue) {
        Boolean oracle;
        if (propertyValue == null) {
            oracle = null;
        } else {
            propertyValue = propertyValue.trim();
            if (propertyValue.isEmpty()) {
                oracle = null;
            } else {
                oracle = Boolean.valueOf(DATABASE_TYPE_ORACLE.equalsIgnoreCase(propertyValue));
            }
        }
        return oracle;
    }

    /**
     * Returns whether A device class indicates in its properties that it is connected to an Oracle database
     *
     * @param className The device class
     * @return A {@link Boolean}. Can be <code>null</code>.
     * @throws DevFailed If a problem occurred during Tango reading.
     */
    public static Boolean isOracle(final String className) throws DevFailed {
        return isOracleProperty(readStringInDB(className, DATABASE_TYPE_PROPERTY));
    }

    public static String readStringInDB(final String className, final String propertyName) throws DevFailed {
        final Database dbase = ApiUtil.get_db_obj();
        String property = "";
        if (dbase != null) {
            final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
            if (dbdatum != null && !dbdatum.is_empty()) {
                property = dbdatum.extractString();
            }
        }
        return property;
    }

    public static String[] readStringArrayInDB(final String className, final String propertyName) throws DevFailed {
        final Database dbase = ApiUtil.get_db_obj();
        String[] property = new String[0];
        if (dbase != null) {
            final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
            if (dbdatum != null && !dbdatum.is_empty()) {
                property = dbdatum.extractStringArray();
            }
        }
        return property;
    }

    public static boolean readBooleanInDB(final String className, final String propertyName) throws DevFailed {
        final Database dbase = ApiUtil.get_db_obj();
        boolean property = false;
        if (dbase != null) {
            final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
            if (dbdatum != null && !dbdatum.is_empty()) {
                property = dbdatum.extractBoolean();
            }
        }
        return property;
    }

    public static Boolean readBooleanObjectInDB(final String className, final String propertyName) throws DevFailed {
        final Database dbase = ApiUtil.get_db_obj();
        Boolean property = null;
        if (dbase != null) {
            final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
            if (dbdatum != null && !dbdatum.is_empty()) {
                property = Boolean.valueOf(dbdatum.extractBoolean());
            }
        }
        return property;
    }

    public static short readShortInDB(final String className, final String propertyName, final short defaultValue)
            throws DevFailed {

        final Database dbase = ApiUtil.get_db_obj();
        final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
        if (dbdatum.is_empty()) {
            return defaultValue;
        }
        return dbdatum.extractShort();

    }

    public static long readLongInDB(final String className, final String propertyName, final long defaultValue)
            throws DevFailed {
        long result = defaultValue;
        final Database dbase = ApiUtil.get_db_obj();
        final DbDatum dbdatum = dbase.get_class_property(className, propertyName);
        if (!dbdatum.is_empty()) {
            result = dbdatum.extractLong();
        }
        return result;

    }
}
