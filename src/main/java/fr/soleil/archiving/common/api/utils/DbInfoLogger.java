package fr.soleil.archiving.common.api.utils;

public class DbInfoLogger {

    public static final String DEFAULT_DBINFO_VALUE = "-----";
    public static final String DEFAULT_DBORIGIN_VALUE = "Default";

    private final String dbType;
    private String dbInfoItem;
    private String dbOrigin;

    public DbInfoLogger(String type) {
        dbType = type;
        dbInfoItem = DEFAULT_DBINFO_VALUE;
        dbOrigin = DEFAULT_DBORIGIN_VALUE;
    }

    public String getDbType() {
        return dbType;
    }

    public String getDbInfoItem() {
        return dbInfoItem;
    }

    public String getDbOrigin() {
        return dbOrigin;
    }

    public void setInfo(String info, String origin) {
        dbInfoItem = info;
        if (!isEmpty(dbInfoItem)) {
            dbOrigin = origin;
        }
    }

    public StringBuilder appendLoggerTrace(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        result.append(dbType).append(" from ").append(dbOrigin).append(" : ").append(dbInfoItem);
        return result;
    }

    public static boolean isEmpty(String infoItem) {
        return infoItem.isEmpty() || infoItem.equals(DEFAULT_DBINFO_VALUE);
    }
}
