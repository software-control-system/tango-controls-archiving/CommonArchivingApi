package fr.soleil.archiving.common.api;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;
import fr.soleil.database.connection.H2DataBaseConnector;
import fr.soleil.database.connection.MySQLDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

/**
 * A class that manages the database connections
 *
 * @author GIRARDOT
 */
public final class ConnectionFactory {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ConnectionFactory.class);
    private static final Map<String, AbstractDataBaseConnector> CONNECTORS = new HashMap<String, AbstractDataBaseConnector>();
    protected static final String DATE_FORMAT_MYSQL = "'%Y-%m-%d %H:%i:%s'";
    protected static final String NOW_MYSQL = "now()";
    protected static final String TO_DATE_MYSQL = "str_to_date(?," + DATE_FORMAT_MYSQL + ")";
    protected static final String DATE_FORMAT_ORACLE = "'YYYY-MM-DD hh24:mi:ss'";
    protected static final String NOW_ORACLE = "sysdate";
    protected static final String TO_DATE_ORACLE = "to_date(?," + DATE_FORMAT_ORACLE + ")";

    /**
     * Tests whether an {@link AbstractDataBaseConnector} is well parametered
     *
     * @param connector The {@link AbstractDataBaseConnector}
     * @throws SQLException If the {@link AbstractDataBaseConnector} was not well parametered
     */
    private static void testConnection(final AbstractDataBaseConnector connector) throws SQLException {
        LOGGER.debug("connecting to db with param={}", ToStringBuilder.reflectionToString(connector));
        final Connection connection = connector.getConnection();
        connector.closeConnection(connection);
    }

    /*
     *  Creates a connection to a database. This connection can then be obtained with {@link #getConnection(String)}
     *
     * @param params All connections parameters
     */
    public static synchronized AbstractDataBaseConnector connect(final DataBaseParameters params)
            throws ArchivingException {
        final AbstractDataBaseConnector connector;
        LOGGER.info("getting connector for db {} with user {}", params.getUrl(), params.getUser());
        if (CONNECTORS.containsKey(params.getUniqueKey())) {
            // share the same pool for the same database
            connector = CONNECTORS.get(params.getUniqueKey());
        } else {
            try {
                if (params.getDbType().equals(DataBaseType.ORACLE)) {
                    connector = new OracleDataBaseConnector(params);
                } else if (params.getDbType().equals(DataBaseType.MYSQL)) {
                    connector = new MySQLDataBaseConnector(params);
				} else if (params.getDbType().equals(DataBaseType.H2)) {
					connector = new H2DataBaseConnector(params);
                } else {
                    connector = connectTryOracleThenMyqsl(params);
                }
                CONNECTORS.put(params.getUniqueKey(), connector);
			} catch (final Exception e) {
                e.printStackTrace();
				throw new ArchivingException(e);
            }
        }
        LOGGER.info("got connector to db {}", params.getUrl());
        return connector;
    }

    private static AbstractDataBaseConnector connectTryOracleThenMyqsl(final DataBaseParameters params)
            throws ArchivingException {
        AbstractDataBaseConnector connector;
        try {
            connector = new OracleDataBaseConnector(params);
            testConnection(connector);
            params.setDbType(DataBaseType.ORACLE);
        } catch (final SQLException e) {
            try {
                connector = new MySQLDataBaseConnector(params);
                testConnection(connector);
                params.setDbType(DataBaseType.MYSQL);
            } catch (final SQLException e1) {
                throw new ArchivingException(e, "create connection");
            }

        }

        return connector;
    }

    /**
     * Returns the query that can be used with an {@link AbstractDataBaseConnector} to get now date
     *
     * @param connector The {@link AbstractDataBaseConnector}
     * @return A {@link String}
     */
    public static String getDateNowQuery(final AbstractDataBaseConnector connector) {
        String query;
        if (connector instanceof OracleDataBaseConnector) {
            query = NOW_ORACLE;
        } else {
            query = NOW_MYSQL;
        }
        return query;
    }

    /**
     * Returns the query that can be used with an {@link AbstractDataBaseConnector} to get a date
     *
     * @param connector The {@link AbstractDataBaseConnector}
     * @return A {@link String}
     */
    public static String getToDateQuery(final AbstractDataBaseConnector connector) {
        String query;
        if (connector instanceof OracleDataBaseConnector) {
            query = TO_DATE_ORACLE;
        } else {
            query = TO_DATE_MYSQL;
        }
        return query;
    }

}
