package fr.soleil.archiving.common.api.exception;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;

public class ArchivingException extends Exception {

    private static final long serialVersionUID = -6352227953667787476L;

    protected static final String INDEX_START = "\t[";
    protected static final String INDEX_END = "]:\n";
    protected static final String MESSAGE = "Message: ";
    protected static final String ORIGIN = "\t\tOrigin: ";
    protected static final String DESCRIPTION = "\t\tDescription:";
    protected static final String SEVERITY = "\t\tSeverity: ";
    protected static final String REASON = "\t\tReason: ";
    protected static final String LF = "\n";
    protected static final String TAB_QUERY_IN_CHARGE = "\t\tQuery in charge: ";
    protected static final String LF_QUERY_IN_CHARGE = "\nQuery in charge: ";
    protected static final String EMPTY = "";
    protected static final String ARCHIVING_EXCEPTION = "ArchivingException";
    protected static final String SQL_EXCEPTION = "SQLException";

    public static final String ERROR = "ERROR";
    public static final String PANIC = "PANIC";
    public static final String WARNING = "WARNING";

    protected String archExcepMessage;
    protected final List<DevError> devErrorList = new ArrayList<DevError>();

    public Collection<DevError> getDevErrorList() {
        return new ArrayList<DevError>(devErrorList);
    }

    public ArchivingException() {
        super();
        archExcepMessage = EMPTY;
    }

    /**
     * @param message
     */
    public ArchivingException(final String message) {
        super(message);
        archExcepMessage = message;
        final String origin = Thread.currentThread().getStackTrace()[2].toString();
        final DevError devError = new DevError(ARCHIVING_EXCEPTION, ErrSeverity.WARN, message, origin);
        devErrorList.add(devError);
    }

    /**
     * @param message
     */
    public ArchivingException(final SQLException e, final String query) {
        super(e);
        String desc = e.getMessage();
        archExcepMessage = desc;
        desc += LF_QUERY_IN_CHARGE + query;
        final String origin = Thread.currentThread().getStackTrace()[2].toString();
        final DevError devError = new DevError(SQL_EXCEPTION, ErrSeverity.WARN, desc, origin);
        devErrorList.add(devError);
    }

    public ArchivingException(final DevFailed e) {
        super(e);
        archExcepMessage = e.getMessage();
        devErrorList.addAll(Arrays.asList(e.errors));
    }

    public ArchivingException(final Exception e) {
        super(e);
        archExcepMessage = e.getMessage();
        if (e instanceof DevFailed) {
            devErrorList.addAll(Arrays.asList(((DevFailed) e).errors));
        } else {
            addDevError(e, null);
        }
    }

    /**
     * This class can be instanciated when exceptions in the archiving service.
     * Exceptions can be : ConnectionException, ATKException
     */
    public ArchivingException(final String message, final String reason, final ErrSeverity archSeverity,
            final String desc, final String origin) {
        super(message);
        archExcepMessage = message;
        devErrorList.add(new DevError(reason, archSeverity, desc, origin));
    }

    public ArchivingException(final String message, final String reason, final ErrSeverity archSeverity,
            final String desc, final String origin, final Exception e) {
        this(message, reason, null, archSeverity, desc, origin, e);
    }

    public ArchivingException(final String message, final String reason, final String query,
            final ErrSeverity archSeverity, final String desc, final String origin, final Exception e) {
        super(e);
        archExcepMessage = message;
        if (e instanceof DevFailed) {
            final DevError[] errors = ((DevFailed) e).errors;
            if (errors != null) {
                devErrorList.addAll(Arrays.asList(errors));
            }
        } else if (e instanceof ArchivingException) {
            devErrorList.addAll(((ArchivingException) e).getDevErrorList());
        } else {
            addDevError(e, query);
        }
        devErrorList.add(new DevError(reason, archSeverity, desc, origin));
    }

    protected void addDevError(final Exception e, final String query) {
        // A new DevError object is built for the caught exception
        // (Original exception)
        final String addDesc = query == null ? EMPTY : TAB_QUERY_IN_CHARGE + query;
        final String reasonOriginal = e.getMessage() + addDesc;
        final ErrSeverity archSeverityOriginal = ErrSeverity.WARN;
        final String descOriginal = e.getLocalizedMessage();
        final String originOriginal = e.getClass().getName();
        final DevError devErrorOriginal = new DevError(reasonOriginal, archSeverityOriginal, descOriginal,
                originOriginal);
        devErrorList.add(devErrorOriginal);
    }

    public void addStack(final String message, final ArchivingException e) {
        archExcepMessage = message;
        devErrorList.addAll(e.devErrorList);
    }

    public void addStack(final ArchivingException e) {
        devErrorList.addAll(e.devErrorList);
    }

    public void addStack(final String reason, final String desc, final String origin) {
        devErrorList.add(new DevError(reason, ErrSeverity.ERR, desc, origin));
    }

    @Override
    public String getMessage() {
        return archExcepMessage;
    }

    public String getLastExceptionMessage() {
        final String desc = EMPTY;
        if (!devErrorList.isEmpty()) {
            return devErrorList.get(0).desc;
        }
        return desc;
    }

    public String getLastExceptionReason() {
        final String reason = EMPTY;
        if (!devErrorList.isEmpty()) {
            return devErrorList.get(0).reason;
        }
        return reason;
    }

    public String getLastExceptionOrigin() {
        final String origin = EMPTY;
        if (!devErrorList.isEmpty()) {
            return devErrorList.get(0).origin;
        }
        return origin;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(MESSAGE).append(archExcepMessage).append(LF);
        if (devErrorList != null) {
            int i = 0;
            for (final DevError devError : devErrorList) {
                stringBuffer.append(INDEX_START).append(++i).append(INDEX_END);
                stringBuffer.append(REASON).append(devError.reason).append(LF);
                stringBuffer.append(SEVERITY).append(errorSeverityToString(devError.severity)).append(LF);
                stringBuffer.append(DESCRIPTION).append(devError.desc).append(LF);
                stringBuffer.append(ORIGIN).append(devError.origin).append(LF);
            }
        }
        return stringBuffer.toString();
    }

    private String errorSeverityToString(final ErrSeverity errSeverity) {
        if (errSeverity == null) {
            return WARNING;
        }
        switch (errSeverity.value()) {
            case ErrSeverity._ERR:
                return ERROR;
            case ErrSeverity._PANIC:
                return PANIC;
            default:
                return WARNING;
        }

    }

    public boolean isNull() {
        return devErrorList == null || devErrorList.size() == 0;
    }

    public DevFailed toTangoException() {
        return new DevFailed(devErrorList.toArray(new DevError[devErrorList.size()]));
    }

    /**
     * Creates an {@link ArchivingException} from an {@link Exception} (no new {@link ArchivingException} is created
     * when given {@link Exception} is already an {@link ArchivingException})
     *
     * @param e The {@link Exception}
     * @return An {@link ArchivingException}
     */
    public static ArchivingException toArchivingException(final Exception e) {
        ArchivingException exception;
        if (e instanceof ArchivingException) {
            exception = (ArchivingException) e;
        } else {
            exception = new ArchivingException(e);
        }
        return exception;
    }
}
