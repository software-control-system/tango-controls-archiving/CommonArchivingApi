package fr.soleil.database.connection;

import static org.junit.Assert.assertEquals;

import javax.management.MalformedObjectNameException;

//import io.micrometer.core.instrument.Meter;
//import io.micrometer.core.instrument.MeterRegistry;
//import io.micrometer.core.instrument.logging.LoggingMeterRegistry;
import org.junit.Test;

import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;

public class OracleConnectionTest {
    @Test
    public void testJdbiHandle() throws ArchivingException, MalformedObjectNameException {
        final DataBaseParameters params = new DataBaseParameters();
        params.setHost("195.221.4.203");
        params.setUser("hdb");
        params.setPassword("hdb");
        params.setName("TEST11SE");
        params.setSchema("hdb");
        params.setDbType(DataBaseParameters.DataBaseType.ORACLE);
        // MeterRegistry registry = new LoggingMeterRegistry();
        // params.setMetricRegistry(registry);
        AbstractDataBaseConnector hdbConnector = ConnectionFactory.connect(params);
        // for (int i = 0; i < 1; i++) {
        // System.out.println("#### " + i);
        int result = hdbConnector.getJdbi().withHandle(
                handle -> handle.createQuery("SELECT 1 FROM DUAL").map((resultSet, ctx) -> resultSet.getInt(1)).one());
        // System.out.println("result " + result);
        assertEquals(1, result);
        /*  List<Meter> meters = registry.getMeters();
        for (Meter meter : meters) {
            System.out.println(meter.measure().iterator().next());
        }*/

        // }

    }
}
