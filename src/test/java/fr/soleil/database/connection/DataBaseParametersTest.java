package fr.soleil.database.connection;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.utils.GetConf;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GetConf.class)
public class DataBaseParametersTest {

    @Test
    public void testMySQL() {
        final DataBaseParameters params = new DataBaseParameters();
        params.setDbType(DataBaseType.MYSQL);
        params.setHost("host");
        params.setInactivityTimeout(10);
        params.setMaxPoolSize((short) 1);
        params.setMinPoolSize((short) 10);
        params.setUser("user");
        params.setName("HDB");
        assertEquals("jdbc:mysql://host/HDB", params.getUrl());
    }

    @Test
    public void testOracle() {
        final DataBaseParameters params = new DataBaseParameters();
        params.setDbType(DataBaseType.ORACLE);
        params.setHost("host");
        params.setInactivityTimeout(10);
        params.setMaxPoolSize((short) 1);
        params.setMinPoolSize((short) 10);
        params.setUser("user");
        params.setName("HDB");
        assertEquals("jdbc:oracle:thin:@host:1521:HDB", params.getUrl());
    }

    @Test
    public void testClassPropertyOracle() throws DevFailed {
        // configure mocking of tango

        PowerMockito.mockStatic(GetConf.class);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.DATABASE_TYPE_PROPERTY)).willReturn("ORACLE");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.HOST_PROPERTY)).willReturn("host");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.NAME_PROPERTY)).willReturn("name");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.USER_PROPERTY)).willReturn("user");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.PASSWORD_PROPERTY)).willReturn("pwd");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.SCHEMA_PROPERTY)).willReturn("schema");
        final DataBaseParameters params = new DataBaseParameters();
        params.setParametersFromTango("HdbArchiver");
        assertEquals("jdbc:oracle:thin:@host:1521:name", params.getUrl());
        assertEquals("schema", params.getSchema());
    }

    @Test
    public void testClassPropertymysql() throws DevFailed {
        // configure mocking of tango
        PowerMockito.mockStatic(GetConf.class);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.DATABASE_TYPE_PROPERTY)).willReturn("mysql");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.HOST_PROPERTY)).willReturn("host");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.NAME_PROPERTY)).willReturn("name");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.USER_PROPERTY)).willReturn("user");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.PASSWORD_PROPERTY)).willReturn("pwd");
        final DataBaseParameters params = new DataBaseParameters();
        params.setParametersFromTango("HdbArchiver");
        assertEquals("jdbc:mysql://host/name", params.getUrl());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testClassPropertyerrors() throws DevFailed {
        // configure mocking of tango
        PowerMockito.mockStatic(GetConf.class);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.DATABASE_TYPE_PROPERTY)).willReturn("toto");
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.HOST_PROPERTY)).willReturn(null);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.NAME_PROPERTY)).willReturn(null);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.USER_PROPERTY)).willReturn(null);
        BDDMockito.given(GetConf.readStringInDB("HdbArchiver", GetConf.PASSWORD_PROPERTY)).willReturn(null);
        final DataBaseParameters params = new DataBaseParameters();
        params.setParametersFromTango("HdbArchiver");
    }
}
