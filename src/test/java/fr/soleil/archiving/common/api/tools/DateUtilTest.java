package fr.soleil.archiving.common.api.tools;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DateUtil;

public class DateUtilTest {

	@Test
	public void stringUSToMilliTest() throws ArchivingException {
		assertEquals(-1, DateUtil.stringToMilli(null));
		assertEquals(1609455600000l, DateUtil.stringToMilli("2021-01-01"));
		assertEquals(1609455600000l, DateUtil.stringToMilli("2021-01-01 "));
		assertEquals(1609459200000l, DateUtil.stringToMilli("2021-01-01 1"));
		assertEquals(1609459200000l, DateUtil.stringToMilli("2021-01-01 01"));
		assertEquals(1609459200000l, DateUtil.stringToMilli("2021-01-01 01:"));
		assertEquals(1609455660000l, DateUtil.stringToMilli("2021-01-01 00:1"));
		assertEquals(1609455660000l, DateUtil.stringToMilli("2021-01-01 00:01"));
		assertEquals(1609455600000l, DateUtil.stringToMilli("2021-01-01 00:00:"));
		assertEquals(1609455601000l, DateUtil.stringToMilli("2021-01-01 00:00:1"));
		assertEquals(1609455601000l, DateUtil.stringToMilli("2021-01-01 00:00:01"));
		assertEquals(1609455600001l, DateUtil.stringToMilli("2021-01-01 00:00:00.001"));
	}

	@Test
	public void stringFRToMilliTest() throws ArchivingException {
		assertEquals(-1, DateUtil.stringToMilli(null));
		assertEquals(1609455600000l, DateUtil.stringToMilli("01-01-2021"));
		assertEquals(1609455600000l, DateUtil.stringToMilli("01-01-2021 "));
		assertEquals(1609459200000l, DateUtil.stringToMilli("01-01-2021 1"));
		assertEquals(1609459200000l, DateUtil.stringToMilli("01-01-2021 01"));
		assertEquals(1609459200000l, DateUtil.stringToMilli("01-01-2021 01:"));
		assertEquals(1609455660000l, DateUtil.stringToMilli("01-01-2021 00:1"));
		assertEquals(1609455660000l, DateUtil.stringToMilli("01-01-2021 00:01"));
		assertEquals(1609455600000l, DateUtil.stringToMilli("01-01-2021 00:00:"));
		assertEquals(1609455601000l, DateUtil.stringToMilli("01-01-2021 00:00:1"));
		assertEquals(1609455601000l, DateUtil.stringToMilli("01-01-2021 00:00:01"));
		assertEquals(1609455600001l, DateUtil.stringToMilli("01-01-2021 00:00:00.001"));
	}

	@Test
	public void stringToDisplayStringTest() throws ArchivingException {
		assertEquals("01-01-2021 00:00:00.000", DateUtil.stringToDisplayString("2021-01-01"));
		assertEquals("01-01-2021 00:00:00.000", DateUtil.stringToDisplayString("2021-01-01 "));
		assertEquals("01-01-2021 01:00:00.000", DateUtil.stringToDisplayString("2021-01-01 1"));
		assertEquals("01-01-2021 01:00:00.000", DateUtil.stringToDisplayString("2021-01-01 01"));
		assertEquals("01-01-2021 01:00:00.000", DateUtil.stringToDisplayString("2021-01-01 01:"));
		assertEquals("01-01-2021 00:01:00.000", DateUtil.stringToDisplayString("2021-01-01 00:1"));
		assertEquals("01-01-2021 00:01:00.000", DateUtil.stringToDisplayString("2021-01-01 00:01"));
		assertEquals("01-01-2021 00:00:00.000", DateUtil.stringToDisplayString("2021-01-01 00:00:"));
		assertEquals("01-01-2021 00:00:01.000", DateUtil.stringToDisplayString("2021-01-01 00:00:1"));
		assertEquals("01-01-2021 00:00:01.000", DateUtil.stringToDisplayString("2021-01-01 00:00:01"));
		assertEquals("01-01-2021 00:00:00.001", DateUtil.stringToDisplayString("2021-01-01 00:00:00.001"));
	}

}
