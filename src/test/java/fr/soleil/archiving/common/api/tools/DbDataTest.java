package fr.soleil.archiving.common.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @deprecated This test is deprecated as it tests deprecated method {@link DbData#splitDbData()} (TANGOARCH-715).
 */
@Deprecated
public class DbDataTest {

    @Test
    public void mapScalarDevStateTest() {
        int read = 0;
        int write = 1;
        int[] value = { read, write };
        int[] expectedRead = { read };
        int[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_STATE);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_STATE);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumDevStateTest() {
        int[] read = { 0, 1 };
        int[] write = { 1, 0 };
        int[] value = ArrayUtils.addAll(read, write);
        int[] expectedRead = read;
        int[] expectedWrite = write;

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_STATE);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_STATE);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarBooleanTest() {
        boolean read = true;
        boolean write = false;
        boolean[] value = { read, write };
        boolean[] expectedRead = { read };
        boolean[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_BOOLEAN);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_BOOLEAN);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertArrayEquals(expectedRead, (boolean[]) att[0].getTimedData()[0].getValue());
        assertArrayEquals(expectedWrite, (boolean[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertArrayEquals(expectedRead, (boolean[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (boolean[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

    }

    @Test
    public void mapSpectrumBooleanTest() {
        boolean[] read = { true, true };
        boolean[] write = { false, false };
        boolean[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_BOOLEAN);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_BOOLEAN);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (boolean[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (boolean[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (boolean[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (boolean[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarDoubleTest() {
        double read = 2.5;
        double write = 2.5;
        double[] value = { read, write };
        double[] expectedRead = { read };
        double[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_DOUBLE);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (double[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (double[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (double[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (double[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumDoubleTest() {
        double[] read = { 1.5f, 2.6f };
        double[] write = { 6.1f, 5f };
        double[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_DOUBLE);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (double[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (double[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (double[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (double[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarStringTest() {
        String read = "read";
        String write = "write";
        String[] value = { read, write };
        String[] expectedRead = { read };
        String[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_STRING);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_STRING);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertArrayEquals(expectedRead, (String[]) att[0].getTimedData()[0].getValue());
        assertArrayEquals(expectedWrite, (String[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertArrayEquals(expectedRead, (String[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (String[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumStringTest() {
        String[] read = { "read1", "read2" };
        String[] write = { "write1", "write2" };
        String[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_STRING);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_STRING);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (String[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (String[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (String[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (String[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarFloatTest() {
        float read = 1.5f;
        float write = 6.9f;
        float[] value = { read, write };
        float[] expectedRead = { read };
        float[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_FLOAT);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_FLOAT);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (float[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (float[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (float[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (float[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrulFloatTest() {
        float[] read = { 1.5f, 2.6f };
        float[] write = { 6.1f, 5f };
        float[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_FLOAT);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_FLOAT);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (float[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (float[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (float[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (float[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarLong64Test() {
        long read = 1l;
        long write = 6l;
        long[] value = { read, write };
        long[] expectedRead = { read };
        long[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_LONG64);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_LONG64);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertArrayEquals(expectedRead, (long[]) att[0].getTimedData()[0].getValue());
        assertArrayEquals(expectedWrite, (long[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertArrayEquals(expectedRead, (long[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (long[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_LONG64);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_ULONG64);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertArrayEquals(expectedRead, (long[]) att[0].getTimedData()[0].getValue());
        assertArrayEquals(expectedWrite, (long[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertArrayEquals(expectedRead, (long[]) att[1].getTimedData()[0].getValue());

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (long[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumLong64Test() {
        long[] read = { 1l, 2l };
        long[] write = { 6l, 5l };
        long[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_LONG64);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_LONG64);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (long[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (long[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (long[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (long[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_LONG64);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_ULONG64);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (long[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (long[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (long[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (long[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarLongTest() {
        int read = 1;
        int write = 6;
        int[] value = { read, write };
        int[] expectedRead = { read };
        int[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_LONG);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_LONG);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_LONG);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_ULONG);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumLongTest() {
        int[] read = { 1, 2 };
        int[] write = { 6, 5 };
        int[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_LONG);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_LONG);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_LONG);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_ULONG);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (int[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (int[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (int[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapScalarShortTest() {
        short read = 1;
        short write = 6;
        short[] value = { read, write };
        short[] expectedRead = { read };
        short[] expectedWrite = { write };

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_SHORT);
        timedData.setValue(value, null);
        timedData.setX(1);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_SHORT);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_USHORT);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_USHORT);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_UCHAR);
        timedData.setValue(value, null);

        NullableTimedData[] uctimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_UCHAR);
        data.setDataFormat(AttrDataFormat._SCALAR);
        data.setTimedData(uctimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(expectedWrite, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(expectedRead, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(expectedRead, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);
    }

    @Test
    public void mapSpectrumShortTest() {
        short[] read = { 1, 2 };
        short[] write = { 6, 5 };
        short[] value = ArrayUtils.addAll(read, write);

        NullableTimedData timedData = new NullableTimedData();
        timedData.setDataType(TangoConst.Tango_DEV_SHORT);
        timedData.setValue(value, null);
        timedData.setX(2);

        NullableTimedData[] timedDataArray = { timedData };
        DbData data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_SHORT);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(timedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        DbData[] att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_USHORT);
        timedData.setValue(value, null);

        NullableTimedData[] utimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_USHORT);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(utimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

        timedData.setDataType(TangoConst.Tango_DEV_UCHAR);
        timedData.setValue(value, null);

        NullableTimedData[] uctimedDataArray = { timedData };
        data = new DbData(ObjectUtils.EMPTY_STRING);
        data.setDataType(TangoConst.Tango_DEV_UCHAR);
        data.setDataFormat(AttrDataFormat._SPECTRUM);
        data.setTimedData(uctimedDataArray);

        data.setWritable(AttrWriteType._READ_WRITE);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertTrue(Arrays.equals(write, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._WRITE);
        att = data.splitDbData();

        assertNull(att[0]);
        assertTrue(Arrays.equals(read, (short[]) att[1].getTimedData()[0].getValue()));

        data.setWritable(AttrWriteType._READ);
        att = data.splitDbData();

        assertTrue(Arrays.equals(read, (short[]) att[0].getTimedData()[0].getValue()));
        assertNull(att[1]);

    }

}
