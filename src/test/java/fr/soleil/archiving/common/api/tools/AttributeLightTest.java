package fr.soleil.archiving.common.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;

public class AttributeLightTest {
	String attName = "fakName";
	int attId = 0;
	String[] values = new String[] { attName, String.valueOf(attId),
			String.valueOf(TangoConst.Tango_DEV_DOUBLE),
			String.valueOf(AttrDataFormat._SPECTRUM),
			String.valueOf(AttrWriteType._WRITE) };

	@Test
	public void constructorTest() {

		AttributeLight attLight = new AttributeLight();
		assertEquals("", attLight.getAttributeCompleteName());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLight(attName);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLight(values);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(TangoConst.Tango_DEV_DOUBLE, attLight.getDataType());
		assertEquals(AttrDataFormat._SPECTRUM, attLight.getDataFormat());
		assertEquals(AttrWriteType._WRITE, attLight.getWritable());
	}


	@Test
	public void toArrayTest() {
		AttributeLight attLight = new AttributeLight(values);
		assertArrayEquals(values, attLight.toArray());

	}

	@Test
	public void equalsTest() {
		AttributeLight attLight = new AttributeLight(values);
		AttributeLight attLightCopy = new AttributeLight(values);
		assertTrue(attLight.equals(attLight));
		assertTrue(attLight.equals(attLightCopy));

		Object object = new Object();
		assertFalse(attLight.equals(object));

		attLightCopy = new AttributeLight(values);
		attLightCopy.setDataFormat(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = new AttributeLight(values);
		attLightCopy.setDataType(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = new AttributeLight(values);
		attLightCopy.setWritable(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = new AttributeLight(values);
		attLightCopy.setAttributeCompleteName("Pink Elephant");
		assertFalse(attLight.equals(attLightCopy));

	}


}
