package fr.soleil.archiving.common.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;

public class AttributeHeavyTest {

	String attName = "fakName";
	String[] lightvalues = new String[] { attName, String.valueOf(0) // attId
			, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
			String.valueOf(AttrDataFormat._SPECTRUM), String.valueOf(AttrWriteType._WRITE) };





	@Test
	public void constructorTest() {

		AttributeHeavy attLight = new AttributeHeavy();
		assertEquals("", attLight.getAttributeCompleteName());
		assertNull(attLight.getRegistration_time());
		assertEquals("", attLight.getAttribute_device_name());
		assertEquals("", attLight.getDomain());
		assertEquals("", attLight.getFamily());
		assertEquals("", attLight.getMember());
		assertEquals("", attLight.getAttribute_name());
		assertEquals("", attLight.getCtrl_sys());
		assertEquals("", attLight.getDescription());
		assertEquals("", attLight.getLabel());
		assertEquals("", attLight.getUnit());
		assertEquals("", attLight.getStandard_unit());
		assertEquals("", attLight.getDisplay_unit());
		assertEquals("", attLight.getFormat());
		assertEquals("", attLight.getMin_value());
		assertEquals("", attLight.getMax_value());
		assertEquals("", attLight.getMin_alarm());
		assertEquals("", attLight.getMax_alarm());
		assertEquals("", attLight.getOptional_properties());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());
		assertEquals(0, attLight.getMax_dim_x());
		assertEquals(0, attLight.getMax_dim_y());
		assertEquals(0, attLight.getLevel());
		assertEquals(0, attLight.getArchivable());
		assertEquals(0, attLight.getSubstitute());

		attLight = new AttributeHeavy(attName);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertNull(attLight.getRegistration_time());
		assertEquals("", attLight.getAttribute_device_name());
		assertEquals("", attLight.getDomain());
		assertEquals("", attLight.getFamily());
		assertEquals("", attLight.getMember());
		assertEquals("", attLight.getAttribute_name());
		assertEquals("", attLight.getCtrl_sys());
		assertEquals("", attLight.getDescription());
		assertEquals("", attLight.getLabel());
		assertEquals("", attLight.getUnit());
		assertEquals("", attLight.getStandard_unit());
		assertEquals("", attLight.getDisplay_unit());
		assertEquals("", attLight.getFormat());
		assertEquals("", attLight.getMin_value());
		assertEquals("", attLight.getMax_value());
		assertEquals("", attLight.getMin_alarm());
		assertEquals("", attLight.getMax_alarm());
		assertEquals("", attLight.getOptional_properties());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());
		assertEquals(0, attLight.getMax_dim_x());
		assertEquals(0, attLight.getMax_dim_y());
		assertEquals(0, attLight.getLevel());
		assertEquals(0, attLight.getArchivable());
		assertEquals(0, attLight.getSubstitute());

		attLight = initHeavy();

	}

	private AttributeHeavy initHeavy() {
		final AttributeHeavy attributeHeavy = new AttributeHeavy(attName);
		attributeHeavy.setRegistration_time(new Timestamp(0l));
		attributeHeavy.setDomain("domain");
		attributeHeavy.setFamily("family");
		attributeHeavy.setMember("member");
		attributeHeavy.setAttribute_name("attName");
		attributeHeavy.setDataType(TangoConst.Tango_DEV_DOUBLE);
		attributeHeavy.setDataFormat(AttrDataFormat._SPECTRUM);
		attributeHeavy.setWritable(AttrWriteType._WRITE);
		attributeHeavy.setMax_dim_x(100);
		attributeHeavy.setMax_dim_y(200);
		attributeHeavy.setLevel(10);
		attributeHeavy.setCtrl_sys("host");
		attributeHeavy.setArchivable(0);
		attributeHeavy.setSubstitute(0);
		attributeHeavy.setDescription("descrition");
		attributeHeavy.setLabel("label");
		attributeHeavy.setUnit("unit");
		attributeHeavy.setStandard_unit("standard_unit");
		attributeHeavy.setDisplay_unit("display_unit");
		attributeHeavy.setFormat("format");
		attributeHeavy.setMin_value("min_value");
		attributeHeavy.setMax_value("max_value");
		attributeHeavy.setMin_alarm("min_alarm");
		attributeHeavy.setMax_alarm("max_alarm");

		return attributeHeavy;
	}

	@Test
	public void toStringTest() {

		AttributeHeavy attLight = initHeavy();
		String expected = "	name .......................fakName\n" + "	**** Donn�es propres � l'ADT... **** \n"
				+ "	Registration Time ..........1970-01-01 01:00:00.0\n" + "	Attribute_complete_name ....fakName\n"
				+ "	Device name ................\n" + "	Domain .....................domain\n"
				+ "	Family .....................family\n" + "	Member .....................member\n"
				+ "	Attribute name .............attName\n" + "	Data type property .........5\n"
				+ "	Data format property .......1\n" + "	Writable property ..........2\n"
				+ "	Maximum X dimension ........100\n" + "	Maximum X dimension ........200\n"
				+ "	Display level ..............0\n" + "	Ctrl system ................host\n"
				+ "	Archivable  ................0\n" + "	Substitute .................0\n"
				+ "	**** Donn�es propres � l'APT... **** \n" + "	Description ................descrition\n"
				+ "	Label ......................label\n" + "	Unit .......................unit\n"
				+ "	Standard Unit ..............standard_unit\n" + "	Display_unit ...............display_unit\n"
				+ "	Format .....................format\n" + "	Min_value ..................min_value\n"
				+ "	Max_value ..................max_value\n" + "	Min_alarm ..................min_alarm\n"
				+ "	Max_alarm ..................max_alarm\n" + "	Optional_properties ........\n"
				+ "	**** Donn�es propres � l'AMT... **** \n";
		System.out.println(attLight.toString());
		assertEquals(expected, attLight.toString());

	}

	@Test
	public void toArrayTest() {
		AttributeHeavy attLight = initHeavy();
		assertArrayEquals(lightvalues, attLight.toArray());

	}

	@Test
	public void equalsTest() {
		AttributeHeavy attLight = initHeavy();
		AttributeHeavy attLightCopy = initHeavy();
		assertTrue(attLight.equals(attLight));
		assertTrue(attLight.equals(attLightCopy));

		Object object = new Object();
		assertFalse(attLight.equals(object));

		attLightCopy = initHeavy();
		attLightCopy.setDataFormat(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = initHeavy();
		attLightCopy.setDataType(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = initHeavy();
		attLightCopy.setWritable(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = initHeavy();
		attLightCopy.setAttributeCompleteName("Pink Elephant");
		assertFalse(attLight.equals(attLightCopy));

	}

}
